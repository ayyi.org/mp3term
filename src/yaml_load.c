/*
 +----------------------------------------------------------------------+
 | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
 | copyright (C) 2018-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib.h>
#include <gio/gio.h>
#include <yaml.h>
#include "debug/debug.h"
#include "yaml/load.h"
#include "yaml/utils.h"
#include "yaml_load.h"

static FILE* open_file  ();
static void  unexpected (yaml_event_t*);

#define get_expected_event(parser, event, EVENT_TYPE) \
	if(!yaml_parser_parse(parser, event)) return false; \
	if((event)->type != EVENT_TYPE) return false;


static bool
get_string_value (yaml_parser_t* parser, yaml_event_t* event)
{
	get_expected_event(parser, event, YAML_SCALAR_EVENT);

	return true;
}


bool
name_handler (yaml_parser_t* parser, const yaml_event_t* _, gpointer arg)
{
	yaml_event_t event;
	get_string_value(parser, &event);
	((GDBusArgInfo*)arg)->name = g_strdup((char*)event.data.scalar.value);
	yaml_event_delete(&event);

	return true;
}


#define ADD_ARG() { \
	method->in_args[++a] = g_malloc0(sizeof(GDBusArgInfo)); \
	method->in_args[a]->ref_count = -1; \
	}

/*
 *  Args is a sequencer. When the function returns the current event should be of type YAML_SEQUENCE_END_EVENT
 */
static bool
args_handler (yaml_parser_t* parser, const yaml_event_t* _, gpointer _method)
{
	GDBusMethodInfo* method = _method;

	yaml_event_t event;
	get_expected_event(parser, &event, YAML_SEQUENCE_START_EVENT);
	yaml_event_delete(&event);
	get_expected_event(parser, &event, YAML_MAPPING_START_EVENT);
	yaml_event_delete(&event);

	method->in_args = g_malloc0(sizeof(GDBusArgInfo*) * 8);
	int a = -1;

	ADD_ARG();

	bool type_handler (yaml_parser_t* parser, const yaml_event_t* _, gpointer arg)
	{
		yaml_event_t event;
		get_string_value(parser, &event);
		((GDBusArgInfo*)arg)->signature = g_strdup((char*)event.data.scalar.value);
		yaml_event_delete(&event);

		return true;
	}

	while (true) {
		while (yaml_parser_parse(parser, &event)) {
			switch (event.type) {
				case YAML_SCALAR_EVENT:
					handle_scalar_event(parser, &event, (YamlHandler[]){
						{"name", name_handler, (gpointer)method->in_args[a]},
						{"type", type_handler, (gpointer)method->in_args[a]},
						{NULL}
					});
					break;
				case YAML_MAPPING_END_EVENT:
					break;
				default:
					unexpected(&event);
					return false;
			}
			if (event.type == YAML_MAPPING_END_EVENT) break;
			yaml_event_delete(&event);
		}

		yaml_parser_parse(parser, &event);
		switch (event.type) {
			case YAML_MAPPING_START_EVENT:
				ADD_ARG();
				break;
			case YAML_MAPPING_END_EVENT:
				unexpected(&event);
				return false;
			case YAML_SEQUENCE_END_EVENT:
				return true;
			default:
				unexpected(&event);
				return false;
		}
		yaml_event_delete(&event);
	}
	return false;
}


#define ADD_METHOD() ({ \
	m++; \
	GDBusMethodInfo* method = g_malloc0(sizeof(GDBusMethodInfo)); \
	method->ref_count = -1; \
	method; \
})


static bool
_parse_methods (yaml_parser_t* parser, const yaml_event_t* _, gpointer _interface)
{
	GDBusInterfaceInfo* interface = _interface;

	yaml_event_t event;
	get_expected_event(parser, &event, YAML_SEQUENCE_START_EVENT);

	GDBusMethodInfo* methods[10] = {0,};
	int m = -1;

	while (yaml_parser_parse(parser, &event)) {
		switch (event.type) {
			case YAML_SCALAR_EVENT:
				handle_scalar_event(parser, &event, (YamlHandler[]){
					{"name", name_handler, (gpointer)methods[m]},
					{"args-in", args_handler, (gpointer)methods[m]},
					{NULL}
				});
				break;
			case YAML_MAPPING_START_EVENT:
				methods[m] = ADD_METHOD();
				break;
			case YAML_MAPPING_END_EVENT:
				yaml_event_delete(&event);
				yaml_parser_parse(parser, &event);
				switch (event.type) {
					case YAML_MAPPING_START_EVENT:
						methods[m] = ADD_METHOD();
						break;
					case YAML_SEQUENCE_END_EVENT:
						goto out;
					default:
						unexpected(&event);
						break;
				}
				break;
			default:
				unexpected(&event);
				break;
		}
		yaml_event_delete(&event);
	}

out:
	;GDBusMethodInfo** methods_ = g_malloc0(sizeof(GDBusMethodInfo*) * (m + 2));
	for(int i = 0; i <= m; i++){
		methods_[i] = methods[i];
	}
	interface->methods = methods_;

	return true;
}


#define ADD_SIGNAL() ({ \
	s++; \
	GDBusSignalInfo* signal = g_malloc0(sizeof(GDBusSignalInfo)); \
	signal->ref_count = -1; \
	signal; \
})


static bool
_parse_signals (yaml_parser_t* parser, const yaml_event_t* _, gpointer _interface)
{
	GDBusInterfaceInfo* interface = _interface;

	interface->signals = g_malloc0(sizeof(GDBusArgInfo*) * 8);
	int s = -1;

	yaml_event_t event;
	get_expected_event(parser, &event, YAML_SEQUENCE_START_EVENT);
	yaml_event_delete(&event);

	while (yaml_parser_parse(parser, &event)) {
		switch (event.type) {
			case YAML_SCALAR_EVENT:
				handle_scalar_event(parser, &event, (YamlHandler[]){
					{"name", name_handler, (gpointer)interface->signals[s]},
					{"args", args_handler, (gpointer)interface->signals[s]},
					{NULL}
				});
				break;
			case YAML_MAPPING_START_EVENT:
				interface->signals[s] = ADD_SIGNAL();
				break;
			case YAML_MAPPING_END_EVENT:
				yaml_event_delete(&event);
				yaml_parser_parse(parser, &event);
				switch (event.type) {
					case YAML_MAPPING_START_EVENT:
						interface->signals[s] = ADD_SIGNAL();
						break;
					case YAML_SEQUENCE_END_EVENT:
						return true;
					default:
						unexpected(&event);
						break;
				}
				break;
			default:
				unexpected(&event);
				break;
		}
		yaml_event_delete(&event);
	}

	return true;
}


static bool
_load_interface_yaml (yaml_parser_t* parser, const yaml_event_t* _, gpointer user_data)
{
	GDBusInterfaceInfo* interface = user_data;
	interface->ref_count = -1;

	int i = 0;
	yaml_event_t event;
	while (yaml_parser_parse(parser, &event) && i++ < 4) {
		switch (event.type) {
			case YAML_SCALAR_EVENT:
				handle_scalar_event(parser, &event, (YamlHandler[]){
					{"name", name_handler, (gpointer)interface},
					{"methods", _parse_methods, (gpointer)interface},
					{"signals", _parse_signals, (gpointer)interface},
					{NULL}
				});
				break;
			default:
				break;
		}
		yaml_event_delete(&event);
	}

	return false;
}


bool
load_interface (const char* filename, GDBusInterfaceInfo* interface)
{
	dbg(1, "%s", filename);

	FILE* fp = open_file(filename);
	return fp
		? yaml_load(fp, (YamlHandler[]){
			{"interface", _load_interface_yaml, interface},
			{NULL}
		})
		: false;
}


static void
unexpected (yaml_event_t* event)
{
	pwarn("unexpected parser event type: %i", event->type);
}


static FILE*
open_file (const char* filename)
{
	char* paths[] = {
		g_strdup_printf("%s/.config/" PACKAGE, g_get_home_dir()),
		g_build_filename(PACKAGE_DATA_DIR "/" PACKAGE, NULL),
		g_build_filename(g_get_current_dir(), NULL),
		g_build_filename(g_get_current_dir(), "src", NULL),
	};

	FILE* fp;
	for(int i=0;i<G_N_ELEMENTS(paths);i++){
		char* path = g_strdup_printf("%s/%s", paths[i], filename);
		fp = fopen(path, "rb");
		g_free(path);
		if(fp) break;
	}
	if(!fp){
		fprintf(stderr, "unable to load interface file %s\n", filename);
	}
	for(int i=0;i<G_N_ELEMENTS(paths);i++){
		g_free(paths[i]);
	}
	return fp;
}

