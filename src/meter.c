/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include <dbus/dbus-glib-lowlevel.h>
#include "yaml_load.h"

#include "resources.c"

static void handle_method_call       (GDBusConnection*, const gchar*, const gchar*, const gchar*, const gchar*, GVariant*, GDBusMethodInvocation*, gpointer);
#if 0
static void print_introspection_data (GDBusNodeInfo*);
static void print_interface_info     (GDBusInterfaceInfo*);
#endif

typedef struct {
	AGlActor*        actor;

	float            level[2];
	float            peak1[2]; // fast
	float            peak2[2]; // slow
	float            max[2];
} Meter;

Meter meter = {0,};

// Currently the interface info is parsed at runtime.
// It would be interesting to create the binary data at build time.
GDBusInterfaceInfo interface = {0,};

#ifdef USE_XML
static const gchar introspection_xml[] =
	"<node>"
	"  <interface name='org.ayyi.alsameter'>"
	"    <method name='level'>"
	"      <arg type='d' name='level0' direction='in'/>"
	"      <arg type='d' name='level1' direction='in'/>"
	"    </method>"
	"    <signal name='ready'/>"
	"  </interface>"
	"</node>";
#endif

static const GDBusInterfaceVTable interface_vtable = {
	&handle_method_call,
	NULL, // get property
	NULL, // set property
};


static void
handle_method_call (GDBusConnection* conn, const gchar* sender, const gchar* object_path, const gchar* interface_name, const gchar* method_name, GVariant* parameters, GDBusMethodInvocation* invocation, gpointer user_data)
{
	if (!g_strcmp0(method_name, "level")) {
		double level[2];
		g_variant_get(parameters, "(dd)", &level[0], &level[1]);
		g_dbus_method_invocation_return_value(invocation, NULL);

		for(int c=0;c<2;c++){
			if(level[c] > meter.level[c])
				meter.level[c] = level[c];
			else
				meter.level[c] = MAX(meter.level[c] * .9, level[c]);
		}

		meter_view_update(meter.actor);
	}
}


static gboolean
on_new_connection (GDBusServer* server, GDBusConnection* connection, gpointer user_data)
{
	g_object_ref(connection);

#ifdef USE_XML
	GDBusNodeInfo* introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);
	GDBusInterfaceInfo* interface_info = g_dbus_node_info_lookup_interface(introspection_data, "org.ayyi.alsameter");
#ifdef DEBUG
	print_interface_info(interface_info);
#endif
#endif

	if(!interface.name){
		load_interface("resources/interface.yaml", &interface);
	}

#if 0
	GBytes* yaml = g_resources_lookup_data ("/org/ayyi/mp3term/resources/interface.yaml", 0, NULL);
	dbg(0, "resource yaml:%s", g_bytes_get_data(yaml, NULL));
	g_bytes_unref (yaml);
#endif

#ifdef DEBUG
#if 0
	print_interface_info(&interface);
#endif
#endif

	guint registration_id = g_dbus_connection_register_object (
		connection,
		"/",
		//introspection_data->interfaces[0],
		&interface,
		&interface_vtable,
		NULL,  /* user_data */
		NULL,  /* user_data_free_func */
		NULL   /* GError** */
	);

	g_assert (registration_id > 0);

	void on_connection_close (GDBusConnection* connection, gboolean remote_peer_vanished, GError* error, gpointer user_data)
	{
		PF;
		g_object_unref(connection);
	}

	g_signal_connect (connection, "closed", G_CALLBACK(on_connection_close), NULL);

	return true;
}


static gpointer
meter_setup_p2p (gpointer _)
{
	PF;

	gchar* guid = g_dbus_generate_guid();
	GError* error = NULL;
	GDBusServer* server = g_dbus_server_new_sync ("unix:abstract=meter", G_DBUS_SERVER_FLAGS_NONE, guid,
		NULL, /* GDBusAuthObserver */
		NULL, /* GCancellable */
		&error
	);
	g_free(guid);

	if(error){
		g_warning ("error setting up peer-to-peer server: %s", error->message);
		g_error_free(error);
		return NULL;
	}

	g_signal_connect (server, "new-connection", G_CALLBACK (on_new_connection), NULL);

	g_dbus_server_start (server);

#ifdef USE_SERVER_THREAD
	GMainLoop* loop = g_main_loop_new (g_main_context_new(), FALSE);
	g_main_loop_run (loop);
#endif

	return NULL;
}


#if 0
static void
print_introspection_data (GDBusNodeInfo* introspection_data)
{
	printf("ref_count=%i\n", introspection_data->ref_count);
	printf("path=%s\n", introspection_data->path);
	printf("interfaces=%p\n", introspection_data->interfaces);
	for(int i=0;i<10;i++){
		GDBusInterfaceInfo* info = introspection_data->interfaces[i];
		if(info){
			print_interface_info (info);
		}
		if(!info) break;
	}

	printf("nodes=%p\n", introspection_data->nodes);
	for(int i=0;i<10;i++){
		GDBusNodeInfo* info = introspection_data->nodes[i];
		printf("    node=%p\n", info);
		if(!info) break;
	}

	printf("annotations=%p\n", introspection_data->annotations);
	for(int i=0;i<10;i++){
		GDBusAnnotationInfo* info = introspection_data->annotations[i];
		printf("    annotations=%p\n", info);
		if(!info) break;
	}
}
#endif


#ifdef DEBUG
#if 0
static void
print_interface_info (GDBusInterfaceInfo* info)
{
	printf("interface:\n");
	printf("    name=%s\n", info->name);
	printf("    methods:\n");
	g_return_if_fail(info->methods);

	void print_args (GDBusArgInfo** args)
	{
		if(args){
			for(int k=0;k<10;k++){
				GDBusArgInfo* arg = args[k];
				if(!arg) break;
				printf("                name=%s\n", arg->name);
				printf("                signature=%s\n", arg->signature);
			}
		}
	}

	for(int j=0;j<10;j++){
		GDBusMethodInfo* method = info->methods[j];
		if(method){
			printf("        method=%p\n", method);
			printf("            name=%s\n", method->name);
			printf("            in=%p\n", method->in_args);
			if(method->in_args){
				print_args(method->in_args);
			}
			printf("            out=%p\n", method->out_args);
			if(method->out_args){
				print_args(method->out_args);
			}
			if(method->annotations){
				printf("            annotations=%p\n", method->annotations);
				for(int l=0;l<10;l++){
					GDBusAnnotationInfo* annotation = method->annotations[l];
					if(!annotation) break;
					printf("                annotation: %s=%s\n", annotation->key, annotation->value);
				}
			}else{
				printf("            annotations: none\n");
			}
		}
		if(!method) break;
	}
	if(info->signals){
		printf("    signals:\n");
		for(int j=0;j<10;j++){
			GDBusSignalInfo* signal = info->signals[j];
			if(!signal) break;
			printf("        name=%s\n", signal->name);
			printf("        args=%p\n", signal->args);
			print_args(signal->args);
			printf("        annotations=%p\n", signal->annotations);
		}
	}else{
		printf("    signals: no\n");
	}
	printf("    properties=%p\n", info->properties);
	printf("    annotations=%p\n", info->annotations);
}
#endif
#endif

