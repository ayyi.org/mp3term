/**
* +----------------------------------------------------------------------+
* | This file is part of imgview                                         |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __container_h__
#define __container_h__

#include "agl/actor.h"

typedef struct {
    AGlActor actor;
} Container;


AGlActorClass* container_get_class ();

AGlActor* container_new (AGlActor*);

#endif
