/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2016-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

// Two shader options: 1) plain shader, 2) meter shader
#undef USE_METER_SHADER

#undef USE_SERVER_THREAD

#include "config.h"
#include "debug/debug.h"
#include "behaviours/material.h"
#include "views/shader.h"
#include "views/meter.h"

#include "../meter.c"

static AGl* agl = NULL;
static int instance_count = 0;

static void     meter_view__free     (AGlActor*);
static void     meter_view__set_size (AGlActor*);
static gboolean inactivity_timeout   (gpointer);

static AGlActorClass actor_class = {0, "Meter", (AGlActorNew*)meter_view, meter_view__free};

typedef struct {
    AGlActor actor;
    guint    timer;
    bool     updated;
} MeterView;

#define MATERIAL(A) ((MaterialBehaviour*)A->behaviours[0])


AGlActorClass*
meter_view_get_class ()
{
	if (!agl) {
		agl = agl_get_instance();

#ifdef USE_METER_SHADER
		agl_actor_class__add_behaviour(&actor_class, material_get_class());
#endif

#ifdef USE_SERVER_THREAD
		if(!g_thread_new("p2p", meter_setup_p2p, NULL)){
			gerr("failed to create p2p thread\n");
		}
#else
		meter_setup_p2p(NULL);
#endif
	}

	return &actor_class;
}


static void
meter_view__init (AGlActor* actor)
{
}


static void
meter_view__free (AGlActor* actor)
{
	MeterView* view = (MeterView*)actor;

	g_free(view);

	if(!--instance_count){
	}
}


static void
meter_view__set_state (AGlActor* actor)
{
}


static inline void
set_uniform_f (AGlShader* shader, int u, float* prev)
{
	AGlUniformInfo* uniform = &shader->uniforms[u];
	if(uniform->value[0] != *prev){
		glUniform1f(uniform->location, uniform->value[0]);
		*prev = uniform->value[0];
	}
}


static bool
meter_view__paint (AGlActor* actor)
{
	MeterView* view = (MeterView*)actor;

	for(int c=0;c<2;c++){
		if(meter.peak2[c] < meter.level[c])
			meter.peak2[c] = meter.level[c];
		else
			meter.peak2[c] *= 0.995;

		if(meter.peak1[c] < meter.level[c])
			meter.peak1[c] = meter.level[c];
		else
			meter.peak1[c] *= 0.98;

		meter.max[c] = MAX(meter.max[c], meter.level[c]);
	}

	float bar_width = agl_actor__width(actor) / 2. - 5.;

#ifdef USE_METER_SHADER
	float h = agl_actor__height(actor);

	static float prev[UNIFORM_MAX] = {0,};

	for(int c=0;c<2;c++){
		meter_shader.uniforms[UNIFORM_HEIGHT].value[0] = h;
		meter_shader.uniforms[UNIFORM_LEVEL].value[0] = meter.level[c] * h;
		meter_shader.uniforms[UNIFORM_PEAK].value[0] = meter.peak1[c] * h;
		meter_shader.uniforms[UNIFORM_PEAK2].value[0] = meter.max[c] * h;

		for(int i=0;i<UNIFORM_MAX;i++){
			set_uniform_f(&meter_shader, i, &prev[i]);
		}

		agl_rect(c * (bar_width + 5.), 0., bar_width, h);
	}
#else
	float height = agl_actor__height(actor);

	AGlRect bar1 = {
		.x = 0.,
		.w = bar_width
	};
	AGlRect bar2 = {
		.x = bar_width + 5.,
		.w = bar_width
	};

	agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], PLAIN_COLOUR2 (agl->shaders.plain) = 0x000000ff);
	agl_rect_((AGlRect){0., 0., agl_actor__width(actor), height});

	float peak0 = height * (1. - meter.level[0]);
	float peak1 = height * (1. - meter.level[1]);

	agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], PLAIN_COLOUR2 (agl->shaders.plain) = 0x55ff5566);
	bar1.y = height * (1. - meter.peak1[0]);
	bar1.h = peak0 - bar1.y;
	agl_rect_(bar1);
	bar2.y = height * (1. - meter.peak1[1]);
	bar2.h = peak1 - bar2.y;
	agl_rect_(bar2);

	agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], PLAIN_COLOUR2 (agl->shaders.plain) = 0x55ff55ff);
	bar1.y = peak0;
	bar1.h = height - peak0;
	agl_rect_(bar1);
	bar2.y = peak1;
	bar2.h = height - peak1;
	agl_rect_(bar2);

	bar1.y = height * (1. - meter.peak2[0]);
	bar1.h = 1.;
	agl_rect_(bar1);
	bar2.y = height * (1. - meter.peak2[1]);
	bar2.h = 1.;
	agl_rect_(bar2);

	agl_set_colour_uniform (&agl->shaders.plain->uniforms[PLAIN_COLOUR], PLAIN_COLOUR2 (agl->shaders.plain) = 0xff6666ff);
	bar1.y = height * (1. - meter.max[0]);
	agl_rect_(bar1);
	bar2.y = height * (1. - meter.max[1]);
	agl_rect_(bar2);
#endif

	view->updated = true;
	if(!view->timer){
		if(meter.peak2[0] > 0.01){
			view->timer = g_timeout_add(80, inactivity_timeout, NULL);
		}
	}

	return true;
}


static void
meter_view__set_size (AGlActor* actor)
{
	actor->region.x2 = agl_actor__width(actor->parent) - 2.;
	actor->region.x1 = actor->region.x2 - METER_WIDTH;
}


AGlActor*
meter_view (AGlActor* _)
{
	meter_view_get_class ();
	instance_count++;

	meter.actor = (AGlActor*)agl_actor__new(MeterView,
		.actor = {
			.class = &actor_class,
#ifndef USE_METER_SHADER
			.program = agl->shaders.plain,
#endif
			.init = meter_view__init,
			.set_state = meter_view__set_state,
			.set_size = meter_view__set_size,
			.paint = meter_view__paint,
		}
	);

#ifdef USE_METER_SHADER
	MATERIAL(meter.actor)->shader = &meter_shader;
#endif

	return meter.actor;
}


void
meter_view_update (AGlActor* actor)
{
	if(agl_actor__width(actor) < 1. || agl_actor__height(actor) < 1.){
		actor->region = (AGlfRegion){agl_actor__width(actor->parent) - METER_WIDTH, 0., agl_actor__width(actor->parent) - 2., agl_actor__height(actor->parent)};
		agl_actor__set_size(actor);
	}

	agl_actor__invalidate(actor);
}


static gboolean
inactivity_timeout (gpointer _)
{
	MeterView* view = (MeterView*)meter.actor;

	if(!view->updated){
		// No clients are updating the meter,
		// so we reset the level and allow the peaks to fall back

		meter.level[0] = meter.level[1] = 0.0;
		meter.peak1[0] -= .01;
		meter.peak1[1] -= .01;
		meter.peak2[0] -= .01;
		meter.peak2[1] -= .01;

		agl_actor__invalidate(meter.actor);

		if(meter.peak2[0] < 0.01){
			meter.peak1[0] = meter.peak1[1] = 0.;
			meter.peak2[0] = meter.peak2[1] = 0.;
			view->timer = 0;

			// hide
			meter.actor->region.x2 = meter.actor->region.x1;
			agl_actor__set_size(meter.actor);

			return G_SOURCE_REMOVE;
		}
	}else{
		view->updated = false;
	}

	return G_SOURCE_CONTINUE;
}

