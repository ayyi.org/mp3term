/**
* +----------------------------------------------------------------------+
* | This file is part of mp3term                                         |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __track_actor_h__
#define __track_actor_h__

#define MAX_TRACKS 4

typedef struct {
    AGlActor actor;
} TracksView;


AGlActorClass* tracks_view_get_class ();

AGlActor* tracks_view       (AGlActor*);
void      tracks_set_tracks (AGlActor*, GCancellable*);

#endif
