/**
* +----------------------------------------------------------------------+
* | This file is part of imgview                                         |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __img_view_h__
#define __img_view_h__

#include "decoder/ad.h"
#include "agl/actor.h"
#include "texture_cache/texture_cache.h"

typedef struct {
    AGlActor actor;
    char*    img;
} ImgView;


AGlActorClass* img_view_get_class ();

AGlActor* img_view               (AGlActor*);
void      img_view_show_img      (ImgView*, char*, GCancellable*);
void      img_view_show_img_data (ImgView*, AdPicture*, char* ref);
AGliSize  img_view_image_size    (ImgView*);

#endif
