/*
 +----------------------------------------------------------------------+
 | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
 | copyright (C) 2018-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 *  Container providing caching and clipping
 */
#include "config.h"
#include "debug/debug.h"
#include "behaviours/cache.h"
#include "behaviours/panel.h"
#include "views/img.h"
#include "views/container.h"

static AGlActorClass actor_class = {0, "Container", (AGlActorNew*)container_new};

#define PANEL(actor) (PanelBehaviour*)actor->behaviours[1]


AGlActorClass*
container_get_class ()
{
	static bool init_done = false;

	if (!init_done) {
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());
		agl_actor_class__add_behaviour(&actor_class, panel_get_class());

		init_done = true;
	}

	return &actor_class;
}


/*
 *   Node size is under the control of a Panel container.
 *   The size constraints are set here, but the final size is
 *   determined by the parent.
 */
static void
container__layout (AGlActor* actor)
{
	PanelBehaviour* panel = PANEL(actor);

	int n_rows = ((int)actor->region.y2) / 200 + ((((int)actor->region.y2) % 200 > 10) ? 1 : 0);
	if (!n_rows) return;

	float x = 0;
	float xr[n_rows][2];
	memset(xr, 0, sizeof(float) * n_rows * 2);
	int n_images = g_list_length(actor->children);
	int n_small = 0;
	for (GList* images=actor->children;images;images=images->next) {
		AGlActor* img = images->data;

		bool is_small = agl_actor__height(img) < 101.;
		if (is_small && n_images < 3) {
			is_small = false;
			img->region.y1 =   0.;
			img->region.y2 = 200.;
		}
		AGliSize size = img_view_image_size((ImgView*)img);

		if (is_small) {
			size.w *= 0.5;

			float* xrows = (float*)xr;

			int x_min = xr[0][0];
			int row = 0;
			if (xr[0][1] < x_min) {
				row = 1;
				x_min = xr[0][1];
			}
			if (n_rows > 1) {
				if (xr[1][0] < x_min) {
					row = 2;
					x_min = xr[1][0];
				}
				if (xr[1][1] < x_min) {
					row = 3;
					x_min = xr[1][1];
				}
			}
			img->region.y1 = 100. * row +   0.;
			img->region.y2 = 100. * row + 100.;
			img->region.x1 = x_min;
			img->region.x2 = img->region.x1 + size.w;
			xrows[row] += size.w;

			n_small++;
		} else {
			img->region.x1 = x;
			img->region.x2 = x + size.w;
			x += size.w;
			xr[0][0] = xr[0][1] = x;
		}
	}

	x = MAX(x, xr[0][0]);
	if (xr[0][1] > 10) x = MIN(x, xr[0][1]);
	if (n_rows > 1) {
		if (xr[1][0] > 10)
			x = MIN(x, xr[1][0]);
		if (xr[1][1] > 10)
			x = MIN(x, xr[1][1]);
	}

	float x_max = 0.f;
	x_max = MAX(x_max, xr[0][0]);
	x_max = MAX(x_max, xr[0][1]);
	if (n_rows > 1) {
		x_max = MAX(x_max, xr[1][0]);
		x_max = MAX(x_max, xr[1][1]);
	}

	panel->min = 0.;
	panel->preferred = x;
	panel->max = x_max;
}


AGlActor*
container_new (AGlActor* _)
{
	container_get_class();

	return (AGlActor*)agl_actor__new(Container,
		.actor = {
			.class = &actor_class,
			.set_size = container__layout,
		},
	);
}

