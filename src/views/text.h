/**
* +----------------------------------------------------------------------+
* | This file is part of imgview                                         |
* | copyright (C) 2018-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __text_actor_h__
#define __text_actor_h__

#include "decoder/ad.h"
#include "texture_cache/texture_cache.h"
#include "metadata.h"

typedef struct {
    AGlActor actor;
    Metadata* metadata;
} TextView;


AGlActorClass* text_view_get_class ();

AGlActor* text_view (AGlActor*);

#endif
