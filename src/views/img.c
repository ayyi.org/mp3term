/*
 +----------------------------------------------------------------------+
 | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
 | copyright (C) 2016-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | IMAGE ACTOR                                                          |
 | Displays an image file from disk.                                    |
 +----------------------------------------------------------------------+
 |
 */

#define USE_TEXTURE_CACHE

#include "config.h"
#include <libgen.h>
#include "debug/debug.h"
#include "agl/behaviours/hover.h"
#include "agl/shader.h"
#include "src/imgload.h"
#include "behaviours/weak-ref.h"
#include "views/img.h"


static AGl* agl = NULL;
static int instance_count = 0;

static void img_view__free (AGlActor*);

static AGlActorClass actor_class = {0, "Img", (AGlActorNew*)img_view, img_view__free};

#define WEAK_REF(A) ((WeakRefBehaviour*)A->behaviours[1])


AGlActorClass*
img_view_get_class ()
{
	if (!agl) {
		agl = agl_get_instance();
		agl_actor_class__add_behaviour(&actor_class, hover_get_class());
		agl_actor_class__add_behaviour(&actor_class, weak_ref_get_class());
	}

	return &actor_class;
}


static void
img_view__init (AGlActor* actor)
{
#ifdef USE_TEXTURE_CACHE
	texture_cache_init_();
#else
	agl_enable(AGL_ENABLE_TEXTURE_2D | AGL_ENABLE_BLEND);
	glGenTextures(1, &view->texture);
#endif
}


static void
img_view__free (AGlActor* actor)
{
	ImgView* view = (ImgView*)actor;

	g_free0(view->img);

	g_free(view);

	if(!--instance_count){
		texture_cache_empty();
	}
}


static void
img_view__set_state (AGlActor* actor)
{
	agl->shaders.texture->uniform.fg_colour = 0xffffffff;
}


static bool
img_view__paint (AGlActor* actor)
{
	// Not currently using an fbo cache.
	// As only one rect is being used, there is unlikely to be any benefit.

	ImgView* view = (ImgView*)actor;

	if(view->img){
		Texture* texture = texture_cache_lookup_(GUINT_TO_POINTER(g_str_hash(view->img)));
		if(texture){
			dbg(1, "%u: %.0f x %.0f", texture->id, agl_actor__width(actor), actor->region.y2);

			float view_aspect = ((float)agl_actor__width(actor)) / agl_actor__height(actor);
			float img_aspect = ((float)texture->size.w) / texture->size.h;
			AGliSize size = {agl_actor__width(actor), agl_actor__height(actor)};

			if(view_aspect > img_aspect){
				size.w *= img_aspect / view_aspect;
			}else if(view_aspect < img_aspect){
				size.h *= view_aspect / img_aspect;
			}
			agl_textured_rect(texture->id, 0, 0, size.w, size.h, NULL);
		}

		if(actor->root->hovered == actor){
			agl_print_with_background(0, 0, 0., 0xffffffff, 0x33aa33ff, "%s", basename(view->img));
		}
	}

	return true;
}


static void
img_view__set_size (AGlActor* actor)
{
}


AGlActor*
img_view (AGlActor* _)
{
	img_view_get_class ();
	instance_count++;

	AGlActor* actor = (AGlActor*)agl_actor__new(ImgView,
		.actor = {
			.class = &actor_class,
			.program = (AGlShader*)agl->shaders.texture,
			.init = img_view__init,
			.set_state = img_view__set_state,
			.set_size = img_view__set_size,
			.paint = img_view__paint,
		},
	);

	WEAK_REF(actor)->ref->actor = actor;

	return actor;
}


static void
img_view_load_img (ImgView* view, char* img, gpointer ref, GCancellable* cancellable)
{
	dbg(1, "%s", img);

	void img_view_load_img_done (Texture* texture, GCancellable* cancellable, gpointer view_ref)
	{
		if (!g_cancellable_is_cancelled(cancellable)) {
			AGlWeakRef* weak_ref = view_ref;
			if (weak_ref->actor)
				agl_actor__set_size((AGlActor*)weak_ref->actor->root);
			weak_ref_unref(weak_ref);
		}
		g_object_unref(cancellable);
	}

	load_img_async (
		&(ImgLoadItem){ .ref = ref, .path = img, .cancellable = g_object_ref(cancellable) },
		img_view_load_img_done,
		weak_ref_ref((AGlActor*)view)
	);
}


void
img_view_show_img (ImgView* view, char* img, GCancellable* cancellable)
{
	AGlActor* actor = (AGlActor*)view;
	g_return_if_fail(view);

	guint ref = g_str_hash(img);

	dbg(1, "%s", img);

	if(view->img) g_free(view->img);
	view->img = g_strdup(img);

	if(actor->name) g_free(actor->name);
	actor->name = g_strdup(basename(img));

	Texture* t = texture_cache_lookup_(GUINT_TO_POINTER(ref));
	if(t){
		texture_cache_freshen_(GUINT_TO_POINTER(ref)); // prevent texture from being purged
		agl_actor__invalidate(actor);
	}else{
		img_view_load_img(view, img, GUINT_TO_POINTER(ref), cancellable);
	}
}


void
img_view_show_img_data (ImgView* view, AdPicture* picture, char* ref)
{
	g_return_if_fail(ref);

	if(view->img) g_free(view->img);

	view->img = g_strdup(ref);

	load_img(&(ImgLoadItem){
		.ref = GUINT_TO_POINTER(g_str_hash(ref)),
		.data = picture->data,
		.size = {
			.w = picture->width,
			.h = picture->height,
		},
		.row_stride = picture->row_stride
	});
	agl_actor__invalidate((AGlActor*)view);
}


AGliSize
img_view_image_size (ImgView* view)
{
	g_return_val_if_fail(view->img, ((AGliSize){0,}));

	Texture* texture = texture_cache_lookup_(GUINT_TO_POINTER(g_str_hash(view->img)));

	return texture
		? texture->size
		: (AGliSize){0,};
}
