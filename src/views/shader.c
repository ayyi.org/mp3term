/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2015-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib.h>
#include "debug/debug.h"
#include "agl/ext.h"
#include "waveform/utils.h"
#include "views/shader.h"

#include "views/shaders/shaders.c"

static AGlUniformInfo uniforms[] = {
   {"height", 1, GL_FLOAT, -1},
   {"level", 1, GL_FLOAT, -1},
   {"peak", 1, GL_FLOAT, -1},
   {"peak2", 1, GL_FLOAT, -1},
   END_OF_UNIFORMS
};


static void
set_uniforms ()
{
}


AGlShader meter_shader = {NULL, NULL, 0, uniforms, set_uniforms, &simple_text};

