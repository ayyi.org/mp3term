/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#undef USE_GTK
#include "config.h"
#include <libgen.h>
#include "debug/debug.h"
#include "waveform/actor.h"
#undef __GDK_H__
#include "application.h"
#include "worker.h"
#include "behaviours/cache.h"
#include "behaviours/panel.h"
#include "views/tracks.h"

int WAV_HEIGHT = 30;

static AGl* agl = NULL;
static WaveformContext* wf_context = NULL;

static AGlActorClass actor_class = {0, "Wavs", (AGlActorNew*)tracks_view};

#define PANEL(actor) (PanelBehaviour*)actor->behaviours[1]


AGlActorClass*
tracks_view_get_class ()
{
	if (!agl) {
		agl = agl_get_instance();
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());
		agl_actor_class__add_behaviour(&actor_class, panel_get_class());
	}

	return &actor_class;
}


static void
tracks_view__init (AGlActor* actor)
{
	static bool init_done = false;

	if(!init_done){
		agl_set_font_string("Roboto 10"); // initialise the pango context

		wf_context = wf_context_new((AGlActor*)actor->root);

		init_done = true;
	}
}


static void
tracks_view__set_state (AGlActor* actor)
{
}


static bool
tracks_view__paint (AGlActor* actor)
{
	agl_set_font_string("Roboto 8");

	for(int i=0;i<app.tracks->len;i++){
		Waveform* w = g_array_index(app.tracks, Waveform*, i);
		agl_print(8, 4 + i * (WAV_HEIGHT + 19), 0, 0xffffffff, basename(w->filename));
	}

	return true;
}


static void
tracks_view__set_size (AGlActor* actor)
{
	PanelBehaviour* panel = PANEL(actor);

	float y = 19.;
	for(GList* l=actor->children;l;l=l->next){
		AGlActor* wav = l->data;
		wav->region = (AGlfRegion){10., y, agl_actor__width(actor) - 20., y + WAV_HEIGHT};
		agl_actor__set_size(wav);
		y += WAV_HEIGHT + 19.;
	}

	if(app.tracks->len){
		panel->preferred = 300.;
		panel->max = 1280.;
	}else{
		panel->preferred = 0.;
		panel->max = 0.;
	}
}


static bool
tracks_view_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
{
	switch(event->type){
		case GDK_SCROLL:
			switch(event->scroll.direction){
				case GDK_SCROLL_UP:
					if(WAV_HEIGHT < 300){
						WAV_HEIGHT += 2;
						agl_actor__invalidate(actor);
						agl_actor__set_size(actor);
					}
					break;
				case GDK_SCROLL_DOWN:
					if(WAV_HEIGHT > 10){
						WAV_HEIGHT -= 2;
						agl_actor__invalidate(actor);
						agl_actor__set_size(actor);
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}

	return AGL_NOT_HANDLED;
}


AGlActor*
tracks_view (AGlActor* actor)
{
	tracks_view_get_class();

	return (AGlActor*)agl_actor__new(TracksView,
		.actor = {
			.class = &actor_class,
			.init = tracks_view__init,
			.set_state = tracks_view__set_state,
			.set_size = tracks_view__set_size,
			.paint = tracks_view__paint,
			.on_event = tracks_view_event,
		}
	);
}


void
tracks_set_tracks (AGlActor* actor, GCancellable* cancellable)
{
	void tracks_on_waveform_set (WaveformActor* wa, gpointer _)
	{
		if(!wa->waveform->renderable) pwarn("load failed");
	}

	void after (GObject* o, GAsyncResult* result, gpointer _actor)
	{
		AGlActor* actor = _actor;

		int n = MIN(app.tracks->len, MAX_TRACKS);
		float y = 19.;
		GList* wavs = actor->children;
		for(int i = 0; i < n; i++, (wavs ? wavs = wavs->next : NULL)){
			Waveform* w = g_array_index(app.tracks, Waveform*, i);
			if(wavs){
				wf_actor_set_waveform((WaveformActor*)wavs->data, w, tracks_on_waveform_set, NULL);
			}else{
				WaveformActor* wa = wf_context_add_new_actor(wf_context, w);
				agl_actor__add_child(actor, (AGlActor*)wa);
				wf_actor_set_region(wa, &(WfSampleRegion){0, waveform_get_n_frames(w)});
				wf_actor_set_rect(wa, &(WfRectangle){10, y, agl_actor__width(actor) - 20, WAV_HEIGHT});
				wf_actor_set_colour(wa, 0x888888ff);
			}
			y += WAV_HEIGHT + 19.;
		}
		tracks_view__set_size(actor);

		for(;wavs;wavs=wavs->next){
			wf_actor_set_waveform((WaveformActor*)wavs->data, NULL, NULL, NULL);
		}
	}

	void _tracks_set_tracks (Task* task, gpointer user_data)
	{
		GCancellable* cancellable = task->cancellable;

		if(!g_cancellable_is_cancelled (cancellable)){

			int n = MIN(app.tracks->len, MAX_TRACKS);

			for(int i = 0; i < n; i++){
				Waveform* waveform = g_array_index(app.tracks, Waveform*, i);
				waveform_get_n_frames(waveform);
			}
		}
		g_object_unref(cancellable);
	}

	g_object_ref(cancellable);
	g_thread_pool_push(pool, AGL_NEW(Task, _tracks_set_tracks, after, actor, cancellable), NULL);
}
