/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __arrange_shader_h__
#define __arrange_shader_h__
#include "agl/utils.h"
#include "agl/shader.h"

#if 0
typedef struct {
	AGlShader      shader;
	struct {
		uint32_t   colour;
		uint32_t   bg_colour;
		AGliPt     centre;
		float      radius;
	}              uniform;
	AGlUniformInfo uniforms[1];
} CircleShader;

enum {
    RING_RADIUS = 0,
    RING_CENTRE,
    RING_COLOUR,
    RING_BG_COLOUR,
};

extern CircleShader circle_shader;
#endif

extern AGlShader meter_shader;

typedef enum
{
   UNIFORM_HEIGHT = 0,
   UNIFORM_LEVEL,
   UNIFORM_PEAK,
   UNIFORM_PEAK2,
   UNIFORM_MAX,
} UniformType;

#endif
