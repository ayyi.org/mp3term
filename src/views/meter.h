/**
* +----------------------------------------------------------------------+
* | This file is part of imgview                                         |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __meter_view_h__
#define __meter_view_h__

#include "agl/actor.h"

#define METER_WIDTH 40.0

AGlActorClass* meter_view_get_class ();

AGlActor* meter_view        (AGlActor*);
void      meter_view_update (AGlActor*);

#endif
