/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include "debug/debug.h"
#include "agl/actor.h"
#include "behaviours/cache.h"
#include "behaviours/panel.h"
#include "views/text.h"


static AGlActorClass actor_class = {0, "Text", (AGlActorNew*)text_view};

#define PANEL(actor) (PanelBehaviour*)actor->behaviours[1]


AGlActorClass*
text_view_get_class ()
{
	static bool init_done = false;

	if(!init_done){
		agl_actor_class__add_behaviour(&actor_class, cache_get_class());
		agl_actor_class__add_behaviour(&actor_class, panel_get_class());
		init_done = true;
	}

	return &actor_class;
}


static void
text_view__init (AGlActor* actor)
{
}


static void
text_view__set_state (AGlActor* actor)
{
}


static bool
text_view__paint (AGlActor* actor)
{
	#define LINE_SPACING 4
	TextView* view = (TextView*)actor;
	Metadata* metadata = view->metadata;

	int y = 8;
	if(metadata->artist){
		agl_set_font_string("Roboto 9");
		agl_print_with_cursor(8, &y, 0, 0xffffffff, "%s", metadata->artist);
		agl_set_font_string("Roboto 30 bold");
		agl_print(8, (y += 6, y), 0, 0xffccaaff, "%s", metadata->album);
		y += 26;
	}
	agl_set_font_string("Mono 8");
	if(metadata->date){
		agl_print_with_cursor(8, &y, 0, 0xffcceeff, metadata->date);
		y += LINE_SPACING + 6;
	}
	if(metadata->nfo){
		gchar** lines = g_strsplit(metadata->nfo, "\n", 20);

		static GRegex* regex = NULL;
		GRegexMatchFlags flags = 0;
		GError* error = NULL;
		if(!regex){
			regex = g_regex_new("\t", 0, flags, &error);
		}
		for(int i=0;i<25;i++){
			gchar* line = lines[i];
			if(!line || y > 190) break;

			gchar* str = g_regex_replace(regex, line, -1, 0, "    ", flags, &error);
			agl_print_with_cursor(8, &y, 0, 0xddddddff, str);
			g_free(str);

			y += LINE_SPACING;
		}
		g_strfreev(lines);
	}

	return true;
}


static void
text_view__layout (AGlActor* actor)
{
	TextView* view = (TextView*)actor;
	Metadata* metadata = view->metadata;
	PanelBehaviour* panel = PANEL(actor);

	panel->min = 0.;
	panel->preferred = 0.;
	panel->max = 400.;

	if(metadata->nfo){
		panel->preferred = 400.;
	}else if(metadata->artist || metadata->date){
		panel->preferred = 200.;
	}
}


AGlActor*
text_view (AGlActor* _)
{
	text_view_get_class();

	return (AGlActor*)agl_actor__new(TextView,
		.actor = {
			.class = &actor_class,
			.init = text_view__init,
			.set_state = text_view__set_state,
			.set_size = text_view__layout,
			.paint = text_view__paint,
		},
	);
}

