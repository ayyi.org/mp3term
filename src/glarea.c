/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "agl/gtk.h"
#include "gtkglext-1.0/gtk/gtkgl.h"
#include "glarea.h"

extern GdkGLConfig* app_get_glconfig ();

#define DIRECT TRUE

typedef struct _GlArea GlArea;
typedef struct _GlAreaClass GlAreaClass;
typedef struct _GlAreaPrivate GlAreaPrivate;

static gpointer gl_area_parent_class = NULL;

GType gl_area_get_type (void) G_GNUC_CONST;
enum  {
	GL_AREA_0_PROPERTY
};

GlArea*         gl_area_new       (void);
GlArea*         gl_area_construct (GType);
static void     gl_area_realize   (GtkWidget*);
static void     gl_area_unrealize (GtkWidget*);
static gboolean gl_area_event     (GtkWidget*, GdkEvent*);


GlArea*
gl_area_construct (GType object_type)
{
	GlArea* area = (GlArea*) g_object_new (object_type, NULL);
	gtk_widget_add_events ((GtkWidget*)area, (gint) ((GDK_KEY_PRESS_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK) | GDK_POINTER_MOTION_MASK | GDK_LEAVE_NOTIFY_MASK));

	return area;
}


GlArea*
gl_area_new (void)
{
	return gl_area_construct (TYPE_GL_AREA);
}


static void
gl_area_realize (GtkWidget* widget)
{
	gtk_widget_set_gl_capability(widget, app_get_glconfig(), agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);

	GTK_WIDGET_CLASS(gl_area_parent_class)->realize (widget);
}


static void
gl_area_unrealize (GtkWidget* widget)
{
	// A reference was not added for the glcontext, so it is not unreferenced here

	if (gtk_widget_get_realized(widget))
		gdk_window_unset_gl_capability(gtk_widget_get_window(widget));

	GTK_WIDGET_CLASS(gl_area_parent_class)->unrealize (widget);
}


static void
gl_area_finalize (GObject* widget)
{
	G_OBJECT_CLASS(gl_area_parent_class)->finalize (widget);
}


static void
gl_area_size_allocate (GtkWidget* widget, GdkRectangle* allocation)
{
	GlArea* area = (GlArea*)widget;
	g_return_if_fail (allocation);

	if(area->scene){
		AGlActor* actor = (AGlActor*)area->scene;
		actor->region = (AGlfRegion){
			.x2 = allocation->width,
			.y2 = allocation->height,
		};
		actor->scrollable = (AGliRegion){
			.x2 = allocation->width,
			.y2 = allocation->height,
		};
		agl_actor__set_size(actor);
	}

	GTK_WIDGET_CLASS(gl_area_parent_class)->size_allocate (widget, allocation);
}


static gboolean
gl_area_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	return agl_actor__on_expose (widget, event, (AGlActor*)((GlArea*)widget)->scene);
}


static void
gl_area_class_init (GlAreaClass* klass)
{
	gl_area_parent_class = g_type_class_peek_parent (klass);

	((GtkWidgetClass*)klass)->realize = gl_area_realize;
	((GtkWidgetClass*)klass)->unrealize = gl_area_unrealize;
	((GtkWidgetClass*)klass)->size_allocate = gl_area_size_allocate;
	((GtkWidgetClass*)klass)->expose_event = gl_area_expose_event;
	((GtkWidgetClass*)klass)->event = gl_area_event;

	((GObjectClass*)klass)->finalize = gl_area_finalize;
}


static void
gl_area_instance_init (GlArea* self)
{
}


GType
gl_area_get_type (void)
{
	static volatile gsize gl_area_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&gl_area_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (GlAreaClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) gl_area_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (GlArea), 0, (GInstanceInitFunc) gl_area_instance_init, NULL };
		GType gl_area_type_id;
		gl_area_type_id = g_type_register_static (GTK_TYPE_DRAWING_AREA, "GlArea", &g_define_type_info, 0);
		g_once_init_leave (&gl_area_type_id__volatile, gl_area_type_id);
	}
	return gl_area_type_id__volatile;
}


static gboolean
gl_area_event (GtkWidget* widget, GdkEvent* event)
{
	g_return_val_if_fail (event, FALSE);

	switch(event->type){
		case GDK_VISIBILITY_NOTIFY:
			break;
		case GDK_CONFIGURE:
			break;
		default:
			dbg(1, "type=%i", event->type);
			break;
	}

	return agl_actor__on_event(((GlArea*)widget)->scene, event);
}

