/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __metadata_h__
#define __metadata_h__

#include "decoder/ad.h"

typedef struct {
    char*     artist;
    char*     title;
    char*     album;
    char*     date;
    int       samplerate;
    AdPicture picture;
    char*     nfo;
} Metadata;

void metadata        (char* path, Metadata*);
void metadata_clear  (Metadata*);

#ifdef HAVE_GLYR
void metadata_glyr   (Metadata*);
void metadata_fetch_coverart (Metadata*);
#endif

#ifdef HAVE_FFMPEG
void metadata_ffmpeg (const char* path, Metadata*);
#endif


#endif
