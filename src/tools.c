/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include "glib.h"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <vte/vte.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"


/*
 *  This sequence is used to create the animated gif on the application homepage
 */
void
demo (VteTerminal* _vte)
{
	static VteTerminal* vte; vte = _vte;

	static int step = 0;
	typedef void (*Step)();
	typedef struct { int delay; Step f; } Item;
	static Item steps[7];

	void next_step ()
	{
		gboolean on_timeout ()
		{
			steps[step].f();

			if(++step < G_N_ELEMENTS(steps))
				next_step();

			return G_SOURCE_REMOVE;
		}
		g_timeout_add(steps[step].delay, on_timeout, NULL);
	}

	gboolean step10 ()
	{
		vte_terminal_feed_child(vte, "q", -1);

		return G_SOURCE_REMOVE;
	}

	gboolean step9 ()
	{
		char* cmd = g_strdup_printf("mplayer *.flac\n");
		vte_terminal_feed_child(vte, cmd, -1);
		g_free(cmd);

		g_timeout_add(5000, step10, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step8 ()
	{
		vte_terminal_feed_child(vte, "ll\n", -1);

		g_timeout_add(900, step9, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step7c ()
	{
		vte_terminal_feed_child(vte, "issimo\\ from\\ Megpoid\n", -1);

		g_timeout_add(500, step8, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step7b ()
	{
		vte_terminal_feed_child(vte, " Gum", -1);

		g_timeout_add(500, step7c, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step7 ()
	{
		vte_terminal_feed_child(vte, "cd", -1);

		g_timeout_add(400, step7b, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step6 ()
	{
		char* cmd = g_strdup_printf("cd ..\n");
		vte_terminal_feed_child(vte, cmd, -1);
		g_free(cmd);

		g_timeout_add(2500, step7, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step5 ()
	{
		char* cmd = g_strdup_printf("q\n");
		vte_terminal_feed_child(vte, cmd, -1);
		g_free(cmd);

		g_timeout_add(1000, step6, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step4 ()
	{
		vte_terminal_feed_child(vte, "mplayer *.flac\n", -1);

		g_timeout_add(3000, step5, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step3 ()
	{
		vte_terminal_feed_child(vte, "ll\n", -1);

		g_timeout_add(1000, step4, NULL);
		return G_SOURCE_REMOVE;
	}

	gboolean step2a3 ()
	{
		vte_terminal_feed_child(vte, "\n", -1);

		g_timeout_add(500, step3, NULL);
		return G_SOURCE_REMOVE;
	}

	void step2_sleeping ()
	{
		vte_terminal_feed_child(vte, "\\ -\\ Sleeping\\ Beauty\\ \\(1979\\)", -1);

		g_timeout_add(500, step2a3, NULL);
	}

	void step2_ra ()
	{
		vte_terminal_feed_child(vte, "\\ \\Ra", -1);
	}

	void step2_sun ()
	{
		vte_terminal_feed_child(vte, " Sun", -1);
	}

	void step2_d ()
	{
		vte_terminal_feed_child(vte, "d", -1);
	}

	void step2_c ()
	{
		vte_terminal_feed_child(vte, "c", -1);
	}

	void step1_mp3 ()
	{
		vte_terminal_feed_child(vte, "cd mp3\n", -1);
	}

	void step0_clear ()
	{
		vte_terminal_feed_child(vte, "cd ~/ && clear\n", -1);
	}

	steps[0] = (Item){   1, step0_clear};
	steps[1] = (Item){1000, step1_mp3};
	steps[2] = (Item){2500, step2_c};
	steps[3] = (Item){ 250, step2_d};
	steps[4] = (Item){ 500, step2_sun};
	steps[5] = (Item){ 500, step2_ra};
	steps[6] = (Item){ 500, step2_sleeping};

	next_step();
}


/*
 *  Show artwork for each subdirectoy
 */
void
each_dir (char* _path, VteTerminal* _vte)
{
	static char* path = NULL;
	static VteTerminal* vte; vte = _vte;

	if(!_path || path) return;

	path = (_path[0] == '~')
		? g_strdup_printf("%s%s", g_get_home_dir(), _path + 1)
		: g_strdup(_path);

	GError* error = NULL;
	static GDir* dir;
	if((dir = g_dir_open(path, 0, &error))){
		gboolean next_dir()
		{
			char filepath[PATH_MAX] = {0,};

			const gchar* file;
			if((file = g_dir_read_name(dir))){
				if(file[0] != '.'){

					snprintf(filepath, PATH_MAX - 1, "%s%c%s", path, G_DIR_SEPARATOR, file);

					if(g_file_test(filepath, G_FILE_TEST_IS_DIR)){
						dbg(0, " * %s", filepath);

						char* cmd = g_strdup_printf("cd \"%s\"\n", filepath);
						vte_terminal_feed_child(vte, cmd, -1);
						g_free(cmd);
					}
				}
			}else{
				g_dir_close(dir);
				g_clear_pointer(&path, g_free);

				return G_SOURCE_REMOVE;
			}

			return G_SOURCE_CONTINUE;
		}
		g_timeout_add(5000, next_dir, NULL);
	}
}

