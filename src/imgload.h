/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __imgload_h__
#define __imgload_h__

#include "agl/utils.h"
#include "texture_cache/texture_cache.h"

typedef guint TextureId;

typedef struct {
    gpointer        ref;
    char*           path;
    uint8_t*        data;
    AGliSize        size;
    int             row_stride;
    Texture*        texture;
    GCancellable*   cancellable;
} ImgLoadItem;

typedef void (*ImgLoadFn) (Texture*, GCancellable*, void*);

Texture* load_img       (ImgLoadItem*);
void     load_img_async (ImgLoadItem*, ImgLoadFn, void*);

#endif
