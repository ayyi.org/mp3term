/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2017-2019 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include "glib.h"
#include "debug/debug.h"
#ifdef HAVE_GLYR
#include <glyr/glyr.h>
#endif
#ifdef HAVE_FFMPEG
#include "decoder/ad.h"
#endif
#include "metadata.h"

#ifdef HAVE_TAGLIB
/*
 *  Taglib can handle picture attachments but does not expose it in the C API.
 *  Taglib is no longer being used as it does not offer anything over FFMpeg
 */
#include "taglib/tag_c.h"
#endif

static char* lookup_tag (WfAudioInfo*, const char* tag);


void
metadata_init ()
{
	static int done = 0;

	if(!done){
#ifdef HAVE_GLYR
		glyr_init();
		atexit (glyr_cleanup);
#endif
		done++;
	}
}


void
metadata_clear (Metadata* m)
{
	g_free0(m->artist);
	g_free0(m->album);
	g_free0(m->title);
	g_free0(m->date);
	g_free0(m->nfo);

	ad_thumbnail_free(NULL, &m->picture);
}


#ifdef HAVE_TAGLIB
void
metadata (char* path, Metadata* metadata)
{
	const TagLib_File* file = taglib_file_new(path);
	if(file){
		TagLib_Tag* tag = taglib_file_tag(file);
		if(tag){
			char* artist = metadata->artist = g_strdup(taglib_tag_artist(tag));
			char* album = metadata->album = g_strdup(taglib_tag_album(tag));
			metadata->title = g_strdup(taglib_tag_title(tag));

			dbg(1, "%s", path);
			dbg(1, "  %s - %s", artist, album);
		}

		const TagLib_AudioProperties* properties = taglib_file_audioproperties(file);
		if(properties){
			metadata->samplerate = taglib_audioproperties_samplerate(properties);
			if(metadata->samplerate != 44100) dbg(0, "  %i", metadata->samplerate);
		}

		taglib_tag_free_strings();
		taglib_file_free((TagLib_File*)file);
	}
}
#endif


#ifdef HAVE_GLYR
static GLYR_ERROR
item_callback (GlyrMemCache* c, GlyrQuery* q)
{
	/*
	 * You can also return:
	 * - GLYRE_STOP_POST which will stop libglyr, but still add the current item
	 * - GLYRE_STOP_PRE  which will stop libglyr, but skip the current item
	 * - GLYRE_SKIP which will cause libglyr not to add this item to the results
	 */
	return GLYRE_OK;
}


static void*
glyr_thread (void* p)
{
	GlyrQuery* q = p;

	//GlyrMemCache * r = glyr_get (p, NULL,NULL);

	GLYR_ERROR err;
	GlyrMemCache* head = glyr_get(q, &err, NULL);

	if (err != GLYRE_OK)
		fprintf(stderr, "%s\n", glyr_strerror(err));

	if(!head)
		dbg(0, "no results");

	while(head){
		glyr_cache_print(head);
		glyr_free_list(head);

		head = head->next;
	}

	glyr_query_destroy(q);
	g_free(q);

	printf("\n");
	return NULL;
}
#endif


#ifdef HAVE_GLYR
void
metadata_fetch_coverart (Metadata* m)
{
	metadata_init();

	GlyrQuery* q = g_new0(GlyrQuery, 1);
	glyr_query_init(q);

	/*
	 * GLYR_GET_ALBUM_REVIEW
	 * GLYR_GET_TRACKLIST
	 * GLYR_GET_ARTIST_PHOTOS
	 */
	glyr_opt_type(q, GLYR_GET_COVERART);

	glyr_opt_verbosity(q, 2);

	// album can be omitted
	glyr_opt_artist (q, (char*)m->artist);
	glyr_opt_album  (q, (char*)m->album);
	glyr_opt_title  (q, (char*)m->title);

	dbg(1, "artist=%s album=%s title=%s", m->artist, m->album, m->title);

	glyr_opt_dlcallback(q, item_callback, NULL);

	g_thread_new ("glyr_thread", glyr_thread, (gpointer)q);
}
#endif


#ifdef HAVE_GLYR
void
metadata_glyr (Metadata* m)
{
	// TODO move to separate thread, see example

	metadata_init();

	if(false){
		GlyrQuery* q = g_new0(GlyrQuery, 1);
		glyr_query_init(q);

		/*
		 * GLYR_GET_ALBUM_REVIEW
		 * GLYR_GET_TRACKLIST
		 * GLYR_GET_ARTIST_PHOTOS
		 */
		//glyr_opt_type(&q, GLYR_GET_COVERART);
		glyr_opt_type(q, GLYR_GET_ALBUM_REVIEW);

		glyr_opt_verbosity(q, 2);

		// album can be omitted
		glyr_opt_artist (q, (char*)m->artist);
		glyr_opt_album  (q, (char*)m->album);
		glyr_opt_title  (q, (char*)m->title);

		glyr_opt_dlcallback(q, item_callback, NULL);

		/*GThread* thread = */g_thread_new ("glyr_thread", glyr_thread, (gpointer)q);
	}
}
#endif


#ifdef HAVE_FFMPEG
void
metadata_ffmpeg (const char* path, Metadata* m)
{
	WfAudioInfo info;
	if(ad_finfo(path, &info)){
#if 0
		ad_print_nfo(5, &info);
#endif
		char* tag;
		if(!m->date){
			tag = lookup_tag(&info, "date");
			if(tag) m->date = g_strdup(tag);
		}

		if((tag = lookup_tag(&info, "album"))){
			if(m->album){
				if(strcmp(m->album, tag)){
					// there is no consistent album name
					g_clear_pointer(&m->album, g_free);
				}
			}else{
				m->album = g_strdup(tag);
			}
		}

		tag = lookup_tag(&info, "artist");
		if(tag){
			if(m->artist && strcmp(m->artist, tag)){
				g_free(m->artist);
				m->artist = g_strdup("Various");
			}else{
				if(m->artist) g_free(m->artist);
				m->artist = g_strdup(tag);
			}
		}

		if(!m->picture.data){
			WfDecoder dec = {0,};
			if(ad_open(&dec, path)){
				ad_thumbnail(&dec, &m->picture);
				ad_close(&dec);
				ad_free_nfo(&dec.info);
			}
			else dbg(1, "open fail");
		}

		ad_free_nfo(&info);
	}
	else dbg(1, "nothing");
}
#endif


static char*
lookup_tag (WfAudioInfo* info, const char* tag)
{
	if(info->meta_data){
		char** tags = (char**)info->meta_data->pdata;
		for(int i=0;i<info->meta_data->len;i+=2){
			if(!strcmp(tags[i], tag))
				return tags[i+1];
		}
	}
	return NULL;
}

