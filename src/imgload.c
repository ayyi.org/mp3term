/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term.                                        |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "agl/ext.h"
#include "debug/debug.h"
#include "imgload.h"
#include "worker.h"

static void buf_to_texture (TextureId, unsigned char* buf, int width, int height, bool alpha);
static void load_png       (char* path, Texture*);
static void load_png_async (char* path, Texture*, GCancellable*, ImgLoadFn, void*);


Texture*
load_img (ImgLoadItem* item)
{
	dbg(1, "%s", item->path);

	if(item->texture)
		return item->texture;

	gpointer hash = item->path
		? GUINT_TO_POINTER(g_str_hash(item->path))
		: ({g_assert(item->ref); item->ref;});

	if((item->texture = texture_cache_lookup_(hash)))
		return item->texture;

	item->texture = texture_cache_assign_new_(hash);

	if(item->path){
		load_png(item->path, item->texture);
	}else{
		glPixelStorei(GL_UNPACK_ROW_LENGTH, item->row_stride / 3);
		const bool alpha = false;
		buf_to_texture (item->texture->id, item->data, item->size.w, item->size.h, alpha);
		item->texture->size = item->size;
		glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
	}

	return item->texture;
}


void
load_img_async (ImgLoadItem* item, ImgLoadFn callback, void* user_data)
{
	dbg(1, "%s", item->path);

	if(item->texture){
		callback(item->texture, item->cancellable, user_data);
		return;
	}

	gpointer hash = item->path
		? GUINT_TO_POINTER(g_str_hash(item->path))
		: ({g_assert(item->ref); item->ref;});

	if((item->texture = texture_cache_lookup_(hash))){
		callback(item->texture, item->cancellable, user_data);
		return;
	}

	item->texture = texture_cache_assign_new_(hash);

	if(item->path){
		load_png_async(item->path, item->texture, item->cancellable, callback, user_data);
		return;
	}

	glPixelStorei(GL_UNPACK_ROW_LENGTH, item->row_stride / 3);
	const bool alpha = false;
	buf_to_texture (item->texture->id, item->data, item->size.w, item->size.h, alpha);
	item->texture->size = item->size;
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

	callback(item->texture, item->cancellable, user_data);
}


static void
buf_to_texture (TextureId tid, unsigned char* buf, int width, int height, bool alpha)
{
	agl_use_texture(tid);

	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, buf);
	gl_warn("texture bind");
}


static GdkPixbuf*
_load_png (char* img, Texture* Xtexture)
{
	GError* error = NULL;
	GdkPixbuf* pixbuf = gdk_pixbuf_new_from_file(img, &error);
	if (!pixbuf) {
		g_warning("%s", error->message);
		g_error_free(error);
		return NULL;
	}

	#define HEIGHT 200
	float scale_ratio = ((float)HEIGHT) / gdk_pixbuf_get_height(pixbuf);

	// Making the width a multiple of 8 prevents the rowstride from being different to the width
	int width = (int)(((int)((float)gdk_pixbuf_get_width(pixbuf) * scale_ratio) + 4) / 8) * 8;

	GdkPixbuf* scaled = gdk_pixbuf_scale_simple(pixbuf, width, HEIGHT, GDK_INTERP_BILINEAR);

	g_object_unref(pixbuf);

	return scaled;
}


static void
load_png (char* img, Texture* texture)
{
	g_return_if_fail(texture);

	GdkPixbuf* scaled = _load_png (img, texture);

	if(scaled){
		int width = gdk_pixbuf_get_width(scaled);
		buf_to_texture(texture->id, gdk_pixbuf_get_pixels(scaled), width, HEIGHT, gdk_pixbuf_get_has_alpha(scaled));
		texture->size = (AGliSize){width, HEIGHT};

		dbg(1, "width=%i rowstride=%i (%i) w=%i has_alpha=%i", texture->size.w, gdk_pixbuf_get_rowstride(scaled), gdk_pixbuf_get_rowstride(scaled) / gdk_pixbuf_get_n_channels(scaled), width, gdk_pixbuf_get_has_alpha(scaled));

		g_object_unref(scaled);
	}
}


static void
load_png_async (char* img, Texture* texture, GCancellable* cancellable, ImgLoadFn callback, void* user_data)
{
	g_return_if_fail(texture);

	typedef struct {
		char* img;
		GdkPixbuf* scaled;
		GCancellable* cancellable;
		ImgLoadFn callback;
		void* user_data;
	} C;

	void img_load_img_blocking (Task* task, gpointer pool_user_data)
	{
		C* c = task->data;

		c->scaled = _load_png (c->img, NULL);

		Texture* t = texture_cache_lookup_(GUINT_TO_POINTER(g_str_hash(c->img)));

		if (t)
			t->size = (AGliSize){gdk_pixbuf_get_width(c->scaled), HEIGHT};
	}

	void img_load_img_post (GObject* o, GAsyncResult* result, gpointer _c)
	{
		C* c = _c;
		Texture* t = NULL;

		if (c->scaled) {
			if ((t = texture_cache_lookup_(GUINT_TO_POINTER(g_str_hash(c->img))))) {
				buf_to_texture(t->id, gdk_pixbuf_get_pixels(c->scaled), t->size.w, HEIGHT, gdk_pixbuf_get_has_alpha(c->scaled));
			}
		}

		c->callback(t, c->cancellable, c->user_data);

		g_clear_pointer(&c->scaled, g_object_unref);
		g_clear_pointer(&c->img, g_free);
		g_free(c);
	}

	g_thread_pool_push (
		pool,
		AGL_NEW(Task, img_load_img_blocking, img_load_img_post, AGL_NEW(C, g_strdup(img), NULL, cancellable, callback, user_data)),
		NULL
	);
}
