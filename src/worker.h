/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2020-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __worker_h__
#define __worker_h__

#include <gio/gio.h>

extern GThreadPool* pool;

void worker_start ();
void worker_stop  ();

typedef struct _Task Task;
typedef void (*TaskFn) (Task*, gpointer);

struct _Task {
    TaskFn              fn;
    GAsyncReadyCallback done;
    void*               data;
    GCancellable*       cancellable;
    int                 iter;
};

#endif
