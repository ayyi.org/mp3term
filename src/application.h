/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __application_h__
#define __application_h__

#include "metadata.h"

void app_init ();
void app_free ();

typedef struct _ApplicationPrivate ApplicationPrivate;

typedef struct {
    Metadata            metadata;
    GArray*             tracks;
    GtkWidget*          menu;
    ApplicationPrivate* priv;
} Application;

#ifdef __application_c__
Application app = {0,};
#else
extern Application app;
#endif

#endif
