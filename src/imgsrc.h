/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __imgsrc_h__
#define __imgsrc_h__

typedef struct
{
	char*      path;
    int        depth;
    GPtrArray* files;
    int        i;
    GPtrArray* subdirs; // array of SourceDir*
} SourceDir;

typedef struct
{
    SourceDir* dir;
} ImageSource;

extern ImageSource imgsrc;

void        imgsrc_set_dir  (char* path);
const char* srcdir_next     (SourceDir*);

#endif
