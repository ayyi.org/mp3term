/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2020-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#include "config.h"
#include "glib.h"
#include "debug/debug.h"
#include "agl/typedefs.h"
#include "worker.h"

GThreadPool* pool = NULL;

void
worker_start ()
{
	void on_thread (gpointer _task, gpointer b)
	{
		Task* task = _task;

		task->fn(task, b);

		gboolean done (gpointer _task)
		{
			Task* task = _task;

			task->done (NULL, NULL, task->data);

			g_free (task);

			return G_SOURCE_REMOVE;
		}

		if(task->done)
			g_idle_add (done, task);
		else
			g_free(task);
	}

	#define max_threads 4
	#define user_data NULL

	pool = g_thread_pool_new (on_thread, user_data, max_threads, false, NULL);
}


#ifdef WITH_VALGRIND
void
worker_stop ()
{
	#define immediate true
	#define wait_ true

	g_thread_pool_free (pool, immediate, wait_);
	pool = NULL;
}
#endif
