/*
 +----------------------------------------------------------------------+
 | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
 | copyright (C) 2018-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <libgen.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <vte/vte.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "waveform/waveform.h"
#include "gtk/utils.h"
#include "agl/gtk.h"
#include "views/img.h"
#include "views/tracks.h"
#include "views/text.h"
#include "views/meter.h"
#include "views/container.h"
#include "behaviours/panel.h"
#include "behaviours/container.h"
#include "glarea.h"
#include "metadata.h"
#include "application.h"
#include "imgsrc.h"
#include "worker.h"
#include "window.h"

struct {
    VteTerminal* vte;
    AGlScene*    scene;
    GtkWidget*   vbox;
    int          user_height;
} window = {0,};

typedef struct {
    int           i;
    GCancellable* cancellable;
    GPtrArray*    images;
    char*         album[3];
    int           content_iter;
    bool          done; // all content retrieved
} Change;

ImageSource imgsrc = {0,};

#define MAX_IMAGES 6 // load more than show due to sorting
#define MAX_SHOW_IMAGES 3
#define MAX_SUBDIR_IMAGES 8
#define IMAGE_SIZE 200.

#define MAX_HISTORY 10
static char* dir_history[MAX_HISTORY + 1] = {NULL,};
static char* dir = NULL;


const char* ignore_types[] = {"m3u", "o", "mp4"};
const char* nfo_types[] = {"nfo", "txt"};
const char* audio_types[] = {"mp3", "flac", "wav", "m4a", "opus"};
const char* non_audio_types[] = {"zip", "rar", "meta", "txt", "log", "cue", "md5", "pdf", "nfo"};

static bool       in_array          (const char*, const char**, int len);
static bool       file_is_image     (const char* path);
static bool       check_for_dupe    (AdPicture*, GPtrArray*);
static GPtrArray* get_dir_info      (const char*, Metadata*);
static gboolean   on_title_change   (gpointer _);
static void       on_title_change_1 (GAsyncReadyCallback, gpointer);
static void       on_title_change_2 (GAsyncReadyCallback, gpointer);
static void       on_title_change_3 (GAsyncReadyCallback, gpointer);
static void       on_title_change_4 (GAsyncReadyCallback, gpointer);
static void       action_refresh    (GtkMenuItem*, gpointer);
static void       set_styles        (GtkWidget*);

typedef void      (*async_fn)       (GAsyncReadyCallback, gpointer);


MenuDef menu_def[] = {
	{"Refresh", G_CALLBACK(action_refresh), GTK_STOCK_REFRESH, true},
};


void
window_new ()
{
	GtkWidget* _window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(_window), "Mp3term");

	gtk_window_set_icon(GTK_WINDOW(_window), gdk_pixbuf_new_from_resource("/org/ayyi/mp3term/resources/mp3term.png", NULL));

	g_signal_connect(G_OBJECT(_window), "delete_event", G_CALLBACK(gtk_main_quit), NULL);

	gtk_window_resize(GTK_WINDOW(_window), 800, 400);

	GtkWidget* vbox = window.vbox = gtk_vpaned_new();
	gtk_paned_set_position((GtkPaned*)vbox, IMAGE_SIZE);
	gtk_container_add(GTK_CONTAINER(_window), vbox);
	set_styles (vbox);

#if 0
	{
		GtkWidget* socket = meter.socket = xembed_socket_new ();
		gtk_box_pack_start(GTK_BOX(vbox), socket, false, false, 0);
		gtk_widget_set_size_request(socket, 320, 100);

		void meter_on_plug_added (XembedSocket* socket, gpointer user_data)
		{
			dbg(0, "socket window id: %x", xembed_socket_get_id(socket));
		}
		g_signal_connect(socket, "plug-added", (GCallback)meter_on_plug_added, NULL);
	}
#endif

	GlArea* area = gl_area_new();
	gtk_paned_add1(GTK_PANED(vbox), (GtkWidget*)area);
	gtk_widget_set_size_request((GtkWidget*)area, 320, IMAGE_SIZE);

	area->scene = window.scene = (AGlScene*)agl_new_scene_gtk((GtkWidget*)area);
	AGlActor* root = (AGlActor*)window.scene;
	root->behaviours[0] = container_behaviour(root);

	bool root_event (AGlActor* actor, GdkEvent* event, AGliPt xy)
	{
		switch(event->type){
			case GDK_BUTTON_PRESS:
				switch(event->button.button){
					case 3:
						if(!app.menu){
							app.menu = gtk_menu_new();
							add_menu_items_from_defn(app.menu, menu_def, G_N_ELEMENTS(menu_def));
							gtk_widget_show_all(app.menu);
						}

						gtk_menu_popup(GTK_MENU(app.menu), NULL, NULL, NULL, NULL, event->button.button, event->button.time);

						return AGL_HANDLED;
				}
				break;
			default:
				break;
		}
		return AGL_NOT_HANDLED;
	}
	root->on_event = root_event;

	agl_actor__add_child(root, container_new(NULL)); // image container

	agl_actor__add_child(root, tracks_view(root));

	AGlActor* text = agl_actor__add_child(root, text_view(NULL));
	((TextView*)text)->metadata = &app.metadata;

	agl_actor__add_child(root, meter_view(NULL));

	window.vte = (VteTerminal*)vte_terminal_new();
	vte_terminal_set_scrollback_lines(window.vte, 1000);
	vte_terminal_set_cursor_blink_mode(window.vte, VTE_CURSOR_BLINK_OFF);
	gtk_paned_add2(GTK_PANED(vbox), (GtkWidget*)window.vte);

#if 0
	gboolean enter ()
	{
#if 0
		extern void each_dir (char*, VteTerminal*);
		each_dir(dir, window.vte);
#endif
		extern void demo (VteTerminal*);
		//demo(window.vte);

		return G_SOURCE_REMOVE;
	}
	g_timeout_add(12000, enter, NULL);
#endif

	void on_event (GObject* object, GdkEvent* event, gpointer _)
	{
		window.user_height = gtk_paned_get_position((GtkPaned*)window.vbox);
	}
	g_signal_connect(G_OBJECT(window.vbox), "notify::position", G_CALLBACK(on_event), NULL);

	GError** error = NULL;
	const char* working_directory = NULL;
	char* user_shell = vte_get_user_shell();
	char* argv[2] = {user_shell,};
	char** envv = NULL;
	GSpawnFlags spawn_flags = 0;
	GSpawnChildSetupFunc child_setup = NULL;
	gpointer child_setup_data = NULL;
	GPid* child_pid = NULL; /* out */

	vte_terminal_fork_command_full(window.vte, VTE_PTY_DEFAULT, working_directory, argv, envv, spawn_flags, child_setup, child_setup_data, child_pid, error);
	g_free(user_shell);

	g_signal_connect(G_OBJECT(window.vte), "child-exited", G_CALLBACK(gtk_main_quit), NULL);

	void _on_title_change (VteTerminal* vte, gpointer _)
	{
		char* _dir = g_strrstr(vte_terminal_get_window_title(vte), ":");
		if(!dir || (_dir && strcmp(_dir + 1, dir))){
			g_idle_add(on_title_change, NULL);
		}
	}

	g_signal_connect(window.vte, "window-title-changed", (GCallback)_on_title_change, NULL);

	void on_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer user_data)
	{
		gtk_widget_set_size_request((GtkWidget*)window.vte, 100, 100);
	}

	g_signal_connect((gpointer)_window, "size-allocate", G_CALLBACK(on_allocate), NULL);

	gtk_widget_show_all(_window);
}


static gboolean
on_title_change (gpointer _)
{
	static async_fn _on_title_change[] = {
		on_title_change_1,
		on_title_change_2,
		on_title_change_3,
		on_title_change_4
	};

	char* _dir = g_strrstr(vte_terminal_get_window_title(window.vte), ":");
	if(!_dir) return G_SOURCE_REMOVE;
	_dir = _dir + 1;

	printf("%s%s: %s%s\n", green, __func__, _dir, ayyi_white);
	dir = (g_free(dir), g_strdup(_dir));

	static GCancellable* cancellable = NULL;
	if(cancellable){
		g_cancellable_cancel (cancellable);
		g_object_unref(cancellable); // any users of the cancellable must add their own reference
	}

	void next_task (GObject* o, GAsyncResult* result, gpointer _c)
	{
		Change* change = _c;

		if(change->done){
			g_object_unref(change->cancellable);
			g_clear_pointer(&change->cancellable, g_object_unref);
			cancellable = NULL;
			g_free(change->album[0]);
			//g_free(album[1]);
			g_free(change);
			return;
		}

		if(++change->i < 4){
			_on_title_change[change->i] (next_task, change);
		}else{
			gboolean again (gpointer change)
			{
				on_title_change_4 (next_task, change);
				return G_SOURCE_REMOVE;
			}

			g_idle_add(again, change);
		}
	}

	next_task(NULL, NULL, AGL_NEW(Change,
		.i = -1,
		.cancellable = g_object_ref(cancellable = g_cancellable_new())
	));

	return G_SOURCE_REMOVE;
}


static char*
get_history ()
{
	char* list = g_strjoinv("','", dir_history);
	if(strlen(list)){
		char* list_ = g_strdup_printf("['%s']", list);
		g_free(list);
		return list_;
	}
	return list;
}


static void
update_history (char* album[])
{
	if(!album[1]) return;

	int p = -1;
	int i = 0; for(;i<MAX_HISTORY;i++){
		if(!dir_history[i]){
			p = i;
			break;
		}
	}
	if(p > -1){
		dir_history[p] = g_strdup_printf("%s - %s", album[0], album[1]);
	}
}


static int
add_more_images (GCancellable* cancellable)
{
	void add_node (const char* path, GCancellable* cancellable)
	{
		AGlActor* root = (AGlActor*)window.scene;

		AGlActor* img = agl_actor__add_child(root->children->data, img_view(NULL));

		img->region.y1 = 0.;
		img->region.y2 = IMAGE_SIZE / 2.;

		img_view_show_img((ImgView*)img, (char*)path, cancellable);
	}

	void item (SourceDir* source, int* n, GCancellable* cancellable)
	{
		const char* path = srcdir_next(source);
		if(path){
			add_node(path, cancellable);
			(*n)++;
		}

		for(int s=source->i;s<source->subdirs->len;s++){
			SourceDir* sub = g_ptr_array_index(source->subdirs, s);
			item(sub, n, cancellable);
			if(*n > 9) break;
		}
		if(*n < 10){
			for(int s = 0; s < source->subdirs->len && (*n < 9); s++){
				SourceDir* sub = g_ptr_array_index(source->subdirs, s);
				item(sub, n, cancellable);
			}
		}
	}

	int n = 0;
	SourceDir* source = imgsrc.dir;
	item(source, &n, cancellable);

	if(n)
		agl_actor__set_size((AGlActor*)window.scene);

	return n;
}


static bool
parse_dir_name (const char* dir, char* out[])
{
	char* base = g_path_get_basename(dir);
	char* separator = g_strrstr(base, " - ");
	if(separator){
		out[0] = g_strndup(base, separator - base);
		out[1] = g_strdup(separator + 3);
		g_free(base);
		return TRUE;
	}
	g_free(base);

	return false;
}


float
find_end_of_smallest_row (AGlActor* actor)
{
	float end[4] = {0,};
	for(GList* l = actor->children; l; l = l->next){
		AGlActor* child = l->data;
		int row = child->region.y1 / 100;
		end[row] = MAX(end[row], child->region.x2);
	}
	int n_rows = ((int)actor->region.y2) / ((int)IMAGE_SIZE) + (((int)actor->region.y2) % ((int)IMAGE_SIZE) > 10 ? 1 : 0);
	if(n_rows == 2)
		return MIN(MIN(MIN(end[0], end[1]), end[2]), end[3]);
	else
		return MIN(end[0], end[1]);
}

/*
 *  Return true if more content was added
 */
static bool
get_more_content (Change* c)
{
	AGlActor* root = (AGlActor*)window.scene;
	AGlActor* image_container = root->children->data;

	GList* last = g_list_last(root->children);
	AGlActor* actor = last->data;
	if(actor->class == meter_view_get_class()){
		last = last->prev;
		actor = last->data;
	}

	float end = find_end_of_smallest_row(image_container);
	// note: the container needs to be overfilled in order to push up the requested width
	if(end < image_container->region.x2 + 300){
		return add_more_images(c->cancellable);
	}

	return false;
}


static bool
is_full ()
{
	AGlActor* root = (AGlActor*)window.scene;
	AGlActor* image_container = root->children->data;

	float end = find_end_of_smallest_row(image_container);
	return end >= image_container->region.x2;
}


static void
on_title_change_4 (GAsyncReadyCallback callback, gpointer _change)
{
	Change* c = _change;
	AGlActor* root = (AGlActor*)window.scene;

	// check if we need more content
	GList* last = g_list_last(root->children);
	AGlActor* actor = last->data;
	if(actor->class == meter_view_get_class()){
		last = last->prev;
		actor = last->data;
	}

	if(c->content_iter++ > 15 || g_cancellable_is_cancelled(c->cancellable)){
		c->done = true;
		goto out;
	}

	if(!get_more_content(c)){
		// All the available content has now been retreived

		if(!is_full()){
				if(root->region.y2 > IMAGE_SIZE + 1.){
					root->region.y2 = IMAGE_SIZE;
					int user_height1 = window.user_height;
					gtk_paned_set_position((GtkPaned*)window.vbox, IMAGE_SIZE);
					window.user_height = user_height1;

					gboolean a (gpointer _)
					{
						agl_actor__set_size((AGlActor*)window.scene);
						return G_SOURCE_REMOVE;
					}
					g_idle_add(a, NULL);
				}
		}else{
				if (root->region.y2 == IMAGE_SIZE){
					int n_images = g_list_length(((AGlActor*)root->children->data)->children);
					if(window.user_height && n_images > 9){
						gtk_paned_set_position((GtkPaned*)window.vbox, window.user_height > 390 ? IMAGE_SIZE * 2 : window.user_height);
					}
				}
		}


		c->done = true;
	}

  out:;
	typedef struct {
		GAsyncReadyCallback callback;
		Change*             change;
	} C;

	void on_title_change_4_blocking (Task* task, gpointer _c)
	{
	}

	void on_title_change_4_post (GObject* o, GAsyncResult* result, gpointer _c)
	{
		C* c = _c;
		if(c->callback) c->callback(o, result, c->change);
		g_free(c);
	}

	g_thread_pool_push (pool, AGL_NEW(Task, on_title_change_4_blocking, on_title_change_4_post, AGL_NEW(C, callback, _change)), NULL);
}


static void
on_title_change_3 (GAsyncReadyCallback callback, gpointer _change)
{
	Change* c = _change;

	update_history(c->album);

	callback(NULL, NULL, _change);
}


static void
on_title_change_2 (GAsyncReadyCallback callback, gpointer _change)
{
	Change* c = _change;
	GPtrArray* images = c->images;

	char* history = get_history();
	if(history && strlen(history)) dbg(1, "history=%s", history);
	g_free(history);

	AGlActor* root = (AGlActor*)window.scene;
	AGlActor* container = root->children->data;

	GList* l = container->children;
	int n = 0;
	for(int i=0; i<images->len; i++){
		char* image = g_ptr_array_index(images, i);

		if(i < MAX_SHOW_IMAGES){
			srcdir_next(imgsrc.dir);

			AGlActor* img = l
				? l->data
				: agl_actor__add_child(container, img_view(NULL));

			img->region.y1 = 0.;
			img->region.y2 = IMAGE_SIZE;

			img_view_show_img((ImgView*)img, image, c->cancellable);

			n++;
			l = l ? l->next : NULL;
		}
	}

	if(app.metadata.picture.data){
		bool dupe = n && check_for_dupe(&app.metadata.picture, images);
		if(!dupe){
			AGlActor* img = l
				? l->data
				: agl_actor__add_child(container, img_view(NULL));

			img->region.y1 = 0.;
			img->region.y2 = IMAGE_SIZE;

			// TODO use the filename for the ref instead of the album name
			img_view_show_img_data((ImgView*)img, &app.metadata.picture, app.metadata.album ? app.metadata.album : imgsrc.dir->path);

			n++;
			l = l ? l->next : NULL;
		}

		ad_thumbnail_free(NULL, &app.metadata.picture);
	}

	for(int i=0;i<images->len;i++){
		char* image = g_ptr_array_index(images, i);
		g_free(image);
	}

	for(int i=0;i<MAX_SUBDIR_IMAGES;){
		int sn = 0;
		for(int s=0;s<imgsrc.dir->subdirs->len && i<MAX_SUBDIR_IMAGES;s++,i++){
			SourceDir* source = g_ptr_array_index(imgsrc.dir->subdirs, s);
			const char* path = srcdir_next(source);
			if(path){
				AGlActor* img = l
					? l->data
					: agl_actor__add_child(container, img_view(NULL));

				img->region.y1 = 0.;
				img->region.y2 = IMAGE_SIZE / 2.;

				img_view_show_img((ImgView*)img, (char*)path, c->cancellable);

				n++;
				l = l ? l->next : NULL;
				sn++;
			}
		}
		if(!sn) break;
	}

	if((!n) && app.metadata.artist && strlen(app.metadata.artist) && app.metadata.album){
#if HAVE_GLYR
		metadata_fetch_coverart(&app.metadata);
#endif
	}

	GList* next = ((AGlActor*)window.scene)->children->next;
	AGlActor* tracks = ((AGlActor*)next->data);
	tracks_set_tracks(tracks, c->cancellable);

	GList* last_valid = g_list_nth(container->children, n - 1); // last_valid is NULL if there are no items
	l = g_list_last(container->children);
	while(l && (l != last_valid)){
		AGlActor* item = l->data;
		l = l->prev;
		agl_actor__remove_child(container, item);
	}

	agl_actor__invalidate(agl_actor__find_by_class(root, text_view_get_class()));

	agl_actor__set_size(root);

	g_ptr_array_free(images, true);

	void on_title_change_2_blocking (Task* task, gpointer _change)
	{
	}

	typedef struct {
		GAsyncReadyCallback callback;
		Change*             change;
	} C;

	void on_title_change_2_post (GObject* o, GAsyncResult* result, gpointer _c)
	{
		C* c = _c;
		c->callback(o, result, c->change);
		g_free(c);
	}

	g_thread_pool_push (pool, AGL_NEW(Task, on_title_change_2_blocking, on_title_change_2_post, AGL_NEW(C, callback, _change)), NULL);
}


static void
on_title_change_1 (GAsyncReadyCallback callback, gpointer user_data)
{
	/*
	 *  Running in worker thread
	 */
	void on_title_change_1_blocking (Task* task, gpointer pool_user_data)
	{
		Change* change = task->data;
		metadata_clear(&app.metadata); // clear any previous metadata

		imgsrc_set_dir(dir);

		change->images = get_dir_info(dir, &app.metadata);

		dbg(1, "n_images=%i", change->images->len);

		if(parse_dir_name(dir, change->album)){
			g_free0(app.metadata.album);
			app.metadata.album = change->album[1];
		}
	}

	g_thread_pool_push (pool, AGL_NEW(Task, on_title_change_1_blocking, callback, user_data), NULL);
}


/*
 *  Collect a list of images and retrieve audio metadata for a directory
 *
 *  The returned array and each element must be freed by the user
 */
static GPtrArray*
get_dir_info (const char* _path, Metadata* m)
{
	bool file_is_audio (const char* path)
	{
		char* extn = g_strrstr(path, ".");

		return extn
			? in_array(extn + 1, audio_types, G_N_ELEMENTS(audio_types))
			: false;
	}

	bool file_is_not_audio (const char* path)
	{
		char* extn = g_strrstr(path, ".");

		return extn
			? in_array(extn + 1, non_audio_types, G_N_ELEMENTS(non_audio_types))
			: true;
	}

	gint image_sort (gconstpointer a, gconstpointer b)
	{
		int score[2];

		for(int i=0;i<2;i++){
			char* image = g_utf8_strdown(*(char* const*)(i ? b : a), -1);
			score[i] =
				g_strrstr(image, "front")
					? 2
					: g_strrstr(image, "cover")
						? 1
						: g_strrstr(image, "cd")
							? -1
							: 0;
			g_free(image);
		}
		return score[1] > score[0] ? 1 : -1;
	}

	int cmpstr (const void *p1, const void *p2)
	{
		return strcmp(* (char * const *) p1, * (char * const *) p2);
	}

#if 0
	int cmpwav (const void* p1, const void* p2)
	{
		Waveform* a = *(Waveform**)p1;
		Waveform* b = *(Waveform**)p2;
		return strcmp(a->filename, b->filename);
	}
#endif

	g_array_remove_range(app.tracks, 0, app.tracks->len);

	GPtrArray* images = g_ptr_array_new();

	char filepath[PATH_MAX] = {'\0',};
	const gchar* file;
	GError* error = NULL;
	GDir* dir;

	char* path = (_path[0] == '~')
		? g_strdup_printf("%s%s", g_get_home_dir(), _path + 1)
		: g_strdup(_path);

	int i = 0;
	if ((dir = g_dir_open(path, 0, &error))) {
		GPtrArray* tracks = g_ptr_array_new();

		while ((file = g_dir_read_name(dir)) && images->len < MAX_IMAGES) {
			if (file[0] == '.') continue;

			snprintf(filepath, PATH_MAX - 1, "%s%c%s", path, G_DIR_SEPARATOR, file);

			if (!g_file_test(filepath, G_FILE_TEST_IS_DIR)) {
				const char* _extn = g_strrstr(file, ".");
				const char* extn = _extn ? _extn + 1 : NULL;
				if(in_array(extn, ignore_types, G_N_ELEMENTS(ignore_types))) continue;

				if(g_file_test(filepath, G_FILE_TEST_IS_SYMLINK) && !g_file_test(filepath, G_FILE_TEST_IS_REGULAR)){
					printf("ignoring dangling symlink: %s\n", filepath);
				}else if(in_array(extn, nfo_types, G_N_ELEMENTS(nfo_types))){
					if(!m->nfo){
						gchar* contents = NULL;
						GError* error = NULL;
						g_file_get_contents(filepath, &contents, NULL, &error);
						if(error){
							pwarn("%s", error->message);
							g_error_free(error);
						}else{
							m->nfo = g_strdelimit(contents, "\r\n", '\n');
						}
					}
				}else{
					if(file_is_image(filepath)){
						char* a = g_strdup(filepath);
						g_ptr_array_add(images, a);
					}else{
#ifdef HAVE_TAGLIB
						metadata(filepath, m);
#endif

#ifdef HAVE_FFMPEG
						if(i < 3){ // get artist twice to see if album is Various
							if(!file_is_not_audio(filepath)){
								metadata_ffmpeg(filepath, m);
							}
						}
#endif

						if(file_is_audio(filepath)){
							g_ptr_array_add(tracks, g_strdup(filepath));
						}
					}
				}
			}

			while (gtk_events_pending())
				gtk_main_iteration();
		}

		g_ptr_array_sort(images, image_sort);
		g_ptr_array_sort(tracks, cmpstr);
		//g_array_sort(app.tracks, cmpwav);

		for(int i=0;i<tracks->len;i++){
			char* path = g_ptr_array_index(tracks, i);
			if(i<MAX_TRACKS){
				Waveform* track = waveform_new(path);
				g_array_append_val(app.tracks, track);
			}
			g_free(path);
		}
		g_ptr_array_free(tracks, TRUE);

		g_dir_close(dir);
	}else{
		if(error->code == 2)
			;//pwarn("%s\n", error->message); // permission denied
		else
			;//perr("cannot open directory. %i: %s\n", error->code, error->message);
		g_error_free0(error);
	}

	g_free(path);

	return images;
}


void
window_free ()
{
	for(GList* l=((AGlActor*)window.scene)->children; l; l=l->next){
		agl_actor__free((AGlActor*)l->data);
	}

	agl_free();
}


static bool
in_array (const char* value, const char** array, int len)
{
	if(value){
		for (int i = 0; i < len; i++) {
			if (!strcmp(value, array[i])) {
				return true;
			}
		}
	}

	return false;
}


static bool
file_is_image (const char* path)
{
	char* extn = g_strrstr(path, ".");
	if(extn == path + strlen(path) - 4){
		if(!strcmp(extn + 1, "jpg")){
			return true;
		}
		if(!strcmp(extn + 1, "png")){
			return true;
		}
	}
	if(extn == path + strlen(path) - 5){
		if(!strcmp(extn + 1, "webp")){
			return true;
		}
	}
	return false;
}


#if 0
static void
buffer_2_png (char* name, guchar* buffer, int width, int height, int rowstride, bool has_alpha)
{
	g_return_if_fail(rowstride >= width);

	GdkPixbuf* pixbuf = gdk_pixbuf_new_from_data(buffer, GDK_COLORSPACE_RGB, has_alpha, 8, width, height, rowstride, NULL, NULL);
	gdk_pixbuf_save(pixbuf, name, "png", NULL, NULL);
	g_object_unref(pixbuf);
}
#endif


static bool
check_for_dupe (AdPicture* picture, GPtrArray* images)
{
	typedef struct {
		int max;
		int in;
		int out;
	} SparseIterator;

	for(int i=0;i<images->len;i++){
		char* image = g_ptr_array_index(images, i);

		Texture* texture = texture_cache_lookup_(GUINT_TO_POINTER(g_str_hash(image)));
		if(texture){
			if((ABS(texture->size.w - picture->width) < 4) && texture->size.h == picture->height){
				int width = MIN(texture->size.w, picture->width);
				#define RGB 3
				#define TSTEP 3
				#define SPARSENESS 4
				int n_mismatches = 0;

				int row_stride = texture->size.w * RGB;
				unsigned char buffer[row_stride * texture->size.h];
				agl_use_texture(texture->id);
				glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);

				int max_rows = picture->height / SPARSENESS;

				#define DIFF_BUFFER_SIZE (max_rows * (picture->width / SPARSENESS))
				guchar diff[DIFF_BUFFER_SIZE];
				memset(diff, 0, DIFF_BUFFER_SIZE);
				int d = 0;
				int total = 0;
				SparseIterator y = {max_rows};
				for(;y.out<y.max;y.out++,y.in+=SPARSENESS){
					int linediff = 0;
					int xt = 0;
					int xp = 0;
					int x = 0;
					for(; x < width; x += SPARSENESS, d++, xt += (RGB * SPARSENESS), xp += (RGB * SPARSENESS)){
						diff[d] =
							ABS(buffer[y.in * row_stride + xt    ] - picture->data[y.in * picture->row_stride + xp    ]) / RGB +
							ABS(buffer[y.in * row_stride + xt + 1] - picture->data[y.in * picture->row_stride + xp + 1]) / RGB +
							ABS(buffer[y.in * row_stride + xt + 2] - picture->data[y.in * picture->row_stride + xp + 2]) / RGB;
						linediff += diff[d];
					}
					total += linediff;
					int pp_diff = linediff / (picture->width / SPARSENESS);
					if(pp_diff < 20){
						dbg(2, "%i: line match (%i %ipp)", y.out, linediff, pp_diff);
					}else{
						printf("%i: line mis-match (%i %ipp)\n", y.out, linediff, pp_diff);
						if(++n_mismatches > 4){
							dbg(0, "too many mismatches %i", n_mismatches);
							return false;
						}
					}
				}
#if 0
				{
					SparseIterator y = {picture->height};

					for(;y.out<y.max;y.out++, y.in += SPARSENESS){
						printf("diff: row %i %i (%i)\n", y.out, y.in, y.out * (picture->width));
						for(int x=0;x<picture->width;x++){
							printf("%3i ", diff[y.out * picture->width + x]);
						}
						printf("\n");
					}
				}
#endif
				int difference_per_pixel = total / d;
				printf("difference=%i (%ipp) mismatching_rows=%i\n", total, difference_per_pixel, n_mismatches);
				g_return_val_if_fail(d == G_N_ELEMENTS(diff), false);
				return true;
			}
			else dbg(0, "size does not match (%i %i). ignoring", texture->size.w, picture->width);
		}
		else pwarn("texture not found");
	}

	return false;
}


static void
action_refresh (GtkMenuItem* widget, gpointer user_data)
{
	on_title_change(NULL);
}


static void
set_styles (GtkWidget* widget)
{
	GtkStyle* style = gtk_style_copy(gtk_widget_get_style(widget));

	style->bg[GTK_STATE_NORMAL] = (GdkColor){0,};
	style->bg[GTK_STATE_PRELIGHT] = (GdkColor){0xffff, 0x5555, 0x5555, 0x5555};

	gtk_widget_set_style(widget, style);
}

