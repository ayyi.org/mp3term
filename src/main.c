/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __main_c__
#include "config.h"
#include <libgen.h>
#include <getopt.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "application.h"
#include "worker.h"
#include "window.h"

static const struct option long_options[] = {
  { "verbose",          1, NULL, 'v' },
  { "add",              1, NULL, 'a' },
  { "help",             0, NULL, 'h' },
  { "version",          0, NULL, 'V' },
};

static const char* const short_options = "b:gv:s:a:p:chV";

static const char* const usage =
	"Usage: %s [OPTIONS]\n\n"
	"Mp3term is a terminal that shows music related information for the current directory.\n" 
	"\n"
	"Options:\n"
	"  -h, --help             show this usage information and quit.\n"
	"  -v, --verbose <level>  show more information.\n"
	"  -V, --version          print version and exit.\n"
	"\n"
	"Report bugs to <tim@orford.org>.\n"
	"Website and manual: <https://gitlab.com/ayyi.org/mp3term>\n"
	"\n";

int
main (int argc, char** argv)
{
	set_log_handlers((char*[]){"Waveform", "Gtk", "GLib", "GLib-GObject", "AGl", "Pango", NULL});

	gtk_init_check(&argc, &argv);

	printf("%s"PACKAGE_NAME". Version "PACKAGE_VERSION"%s\n", yellow, ayyi_white);

	int opt;
	while((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch(opt) {
			case 'v':
				printf("using debug level: %s\n", optarg);
				int d = atoi(optarg);
				if(d<0 || d>5) { pwarn ("bad arg. debug=%i", d); } else _debug_ = d;
				break;
			case 'h':
				printf(usage, basename(argv[0]));
				exit(EXIT_SUCCESS);
				break;
			case 'V':
				printf ("%s %s\n\n", basename(argv[0]), PACKAGE_VERSION);
				printf(
					"Copyright (C) 2018-2019 Tim Orford\n"
					"This is free software; see the source for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n"
					);
				exit(EXIT_SUCCESS);
				break;
			case '?':
			default:
				printf("unknown option: %c\n", optopt);
				printf(usage, basename(argv[0]));
				exit(EXIT_FAILURE);
				break;
		}
	}

	app_init();

	worker_start();
	window_new();

	gtk_main();

#ifdef WITH_VALGRIND
	app_free();
	worker_stop();
#endif

	exit(EXIT_SUCCESS);
}
