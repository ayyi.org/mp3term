/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#define __application_c__
#define USE_GTK
#include "config.h"
#include <gtk/gtkgl.h>
#include <stdlib.h>
#include "debug/debug.h"
#include "agl/actor.h"
#include "waveform/waveform.h"
#include "imgsrc.h"
#include "window.h"
#include "application.h"


struct _ApplicationPrivate {
    GdkGLConfig* glconfig;
};


void
app_init ()
{
	app.priv = g_new0(ApplicationPrivate, 1);

	app.tracks = g_array_new(true, true, sizeof(gpointer));

	void free_track (gpointer p)
	{
		g_object_unref(*((Waveform**)p));
	}
	g_array_set_clear_func(app.tracks, free_track);
}


GdkGLConfig*
app_get_glconfig ()
{
	if(!app.priv->glconfig){
		if(!(app.priv->glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_STENCIL | GDK_GL_MODE_DOUBLE))){
			perr ("Cannot initialise gtkglext");
			return NULL;
		}
	}
	return app.priv->glconfig;
}


#ifdef WITH_VALGRIND
void
app_free ()
{
	metadata_clear(&app.metadata);
	g_array_free(app.tracks, true);

	imgsrc_set_dir(NULL);

	window_free();
}
#endif
