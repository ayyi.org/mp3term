/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#include <stdio.h>
#include <libgen.h>
#include <glib.h>
#include "debug/debug.h"
#include "agl/utils.h"
#include "imgsrc.h"

#define MAX_ITEMS 100
#define MAX_DEPTH 1

static void srcdir_read   (SourceDir*);
static void srcdir_free   (SourceDir**);

static bool file_is_image (const char*);
static gint image_sort    (gconstpointer a, gconstpointer b);


SourceDir*
sourcedir_new (char* filepath, int depth)
{
	SourceDir* dir = AGL_NEW(SourceDir,
		.depth = depth,
		.path = g_strdup(filepath),
		.subdirs = g_ptr_array_new(),
		.files = g_ptr_array_new()
	);
	g_ptr_array_set_free_func(dir->files, g_free);

	return dir;
}


void
imgsrc_set_dir (char* path)
{
	if(imgsrc.dir){
		srcdir_free(&imgsrc.dir);
	}

	if(!path){
		return;
	}

	imgsrc.dir = sourcedir_new(path, 0);

	srcdir_read(imgsrc.dir);
}


static void
srcdir_free (SourceDir** _source)
{
	SourceDir* source = *_source;

	g_clear_pointer(&source->path, g_free);

	g_ptr_array_free(source->files, true);
	source->files = NULL;

	void foreach (gpointer data, gpointer user_data)
	{
		SourceDir* dir = data;
		srcdir_free(&dir);
	}

	if(source->subdirs){
		g_ptr_array_foreach(source->subdirs, foreach, NULL);
	}

	g_ptr_array_free(source->subdirs, true);
	source->subdirs = NULL;

	g_clear_pointer(_source, g_free);
}


const char*
srcdir_next (SourceDir* source)
{
	g_return_val_if_fail(source->files, NULL);

	if(source->i < source->files->len){
		return g_ptr_array_index(source->files, source->i++);
	}

	return NULL;
}


static void
srcdir_read (SourceDir* source)
{
	char filepath[PATH_MAX] = {'\0',};
	const gchar* file;
	GError* error = NULL;
	GDir* dir;

	char* path = (source->path[0] == '~')
		? g_strdup_printf("%s%s", g_get_home_dir(), source->path + 1)
		: g_strdup(source->path);

	if((dir = g_dir_open(path, 0, &error))){
		while((file = g_dir_read_name(dir)) && source->files->len < MAX_ITEMS){
			if(file[0] == '.') continue;

			snprintf(filepath, PATH_MAX - 1, "%s%c%s", path, G_DIR_SEPARATOR, file);

			if(g_file_test(filepath, G_FILE_TEST_IS_DIR)){
				if(source->depth < MAX_DEPTH){
					SourceDir* sub = sourcedir_new(filepath, source->depth + 1);
					srcdir_read(sub);
					if (sub->files->len) {
						g_return_if_fail(source->subdirs);
						g_ptr_array_add(source->subdirs, sub);
					} else
						srcdir_free(&sub);
				}
				continue;
			}

			if(g_file_test(filepath, G_FILE_TEST_IS_SYMLINK) && !g_file_test(filepath, G_FILE_TEST_IS_REGULAR)){
				printf("ignoring dangling symlink: %s\n", filepath);
				continue;
			}

			if(file_is_image(filepath)){
				g_ptr_array_add(source->files, g_strdup(filepath));
			}
		}
		g_dir_close(dir);

		g_ptr_array_sort(source->files, image_sort);
	}else{
		pwarn("%s", error->message);
		g_error_free(error);
	}

	g_free(path);
}


static bool
file_is_image (const char* path)
{
	char* extn = g_strrstr(path, ".");
	if(extn == path + strlen(path) - 4){
		if(!strcmp(extn + 1, "jpg")){
			return true;
		}
		if(!strcmp(extn + 1, "png")){
			return true;
		}
	}
	if(extn == path + strlen(path) - 5){
		if(!strcmp(extn + 1, "webp")){
			return true;
		}
	}
	return false;
}


static gint
image_sort (gconstpointer a, gconstpointer b)
{
	int score[2];

	for(int i=0;i<2;i++){
		char* image = g_utf8_strdown(*(char* const*)(i ? b : a), -1);
		score[i] =
			g_strrstr(image, "front")
				? 2
				: g_strrstr(image, "cover")
					? 1
					: g_strrstr(image, "cd")
						? -1
						: 0;
		g_free(image);
	}
	return score[1] > score[0] ? 1 : -1;
}
