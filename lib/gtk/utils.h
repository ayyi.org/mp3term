/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2007-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

typedef struct _MenuDef
{
	char*     label;
	GCallback callback;
	char*     stock_id;
	bool      sensitive;
} MenuDef;

void add_menu_items_from_defn (GtkWidget* menu, MenuDef*, int n);
