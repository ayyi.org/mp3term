/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2007-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <stdbool.h>
#include <yaml.h>

typedef bool (*YamlCallback) (yaml_parser_t*, const yaml_event_t*, gpointer);

typedef struct
{
	char*        key;
	YamlCallback callback;
	gpointer     data;
} YamlHandler;

bool yaml_load         (FILE*, YamlHandler[]);
void yaml_set_string   (yaml_event_t*, gpointer);
void yaml_set_int      (yaml_event_t*, gpointer);
void yaml_set_uint64   (yaml_event_t*, gpointer);

bool handle_scalar_event (yaml_parser_t*, yaml_event_t*, YamlHandler[]);

