/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2020-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "test/runner.h"
#include "weak-ref.h"

TestFn test_weak_ref;

gpointer tests[] = {
	test_weak_ref,
};


int
setup (int argc, char* argv[])
{
	TEST.n_tests = G_N_ELEMENTS(tests);

	return 0;
}


void
teardown ()
{
}


void
test_weak_ref ()
{
	START_TEST;

	AGlActor* actor = agl_actor__new(AGlActor);
	AGlBehaviour* behaviour = weak_ref_behaviour();
	agl_actor__add_behaviour(actor, behaviour);

	AGlWeakRef* weak_ref = ((WeakRefBehaviour*)behaviour)->ref;
	assert(weak_ref->ref_count == 0, "expected initial ref_count 0");
	weak_ref = weak_ref_ref(actor);
	assert(weak_ref->ref_count == 1, "expected ref_count 1");

	agl_actor__free(actor);
	assert(weak_ref->ref_count == 1, "expected ref count unchanged after unref");
	assert(!weak_ref->actor, "expected ref removed after unref");

	weak_ref_unref(weak_ref);

	FINISH_TEST;
}
