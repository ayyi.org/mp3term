/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2016-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "glib.h"
#include "agl/actor.h"

typedef struct {
   AGlActor* actor;
   int ref_count;
} AGlWeakRef;

typedef struct {
   AGlBehaviour behaviour;
   AGlWeakRef* ref;
} WeakRefBehaviour;

AGlBehaviourClass* weak_ref_get_class ();

AGlBehaviour* weak_ref_behaviour      ();
AGlWeakRef*   weak_ref_ref            (AGlActor*);
void          weak_ref_unref          (AGlWeakRef*);
