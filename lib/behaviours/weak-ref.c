/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2019-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  The weak-ref behaviour allows references to be safely kept for
 |  objects which might later get removed
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "weak-ref.h"

static void weak_ref_free (AGlBehaviour*);

static AGlBehaviourClass klass = {
	.new = weak_ref_behaviour,
	.free = weak_ref_free
};


AGlBehaviourClass*
weak_ref_get_class ()
{
	return &klass;
}


AGlBehaviour*
weak_ref_behaviour ()
{
	return (AGlBehaviour*)AGL_NEW(WeakRefBehaviour,
		.behaviour = {
			.klass = &klass,
		},
		.ref = g_new0(AGlWeakRef, 1)
	);
}


static void
weak_ref_free (AGlBehaviour* behaviour)
{
	if (((WeakRefBehaviour*)behaviour)->ref->ref_count < 1)
		g_free(((WeakRefBehaviour*)behaviour)->ref);
	else
		((WeakRefBehaviour*)behaviour)->ref->actor = NULL;
	((WeakRefBehaviour*)behaviour)->ref = NULL;
	g_free(behaviour);
}


AGlWeakRef*
weak_ref_ref (AGlActor* actor)
{
	WeakRefBehaviour* weak_ref = (WeakRefBehaviour*)agl_actor__find_behaviour(actor, &klass);
	if (weak_ref) {
		weak_ref->ref->ref_count++;
		return weak_ref->ref;
	}
	return NULL;
}


void
weak_ref_unref (AGlWeakRef* weak_ref)
{
	if (--weak_ref->ref_count < 1) {
		if (!weak_ref->actor)
			g_free(weak_ref);
	}
}
