/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. https://ayyi.github.io/samplecat/    |
 | copyright (C) 2019-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  This container behaviour manages the layout of any
 |  direct descendents with the Panel behaviour.
 |
 |  The panel behaviour allows nodes to be layed out using
 |  the properties min, max, and preferred size.
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "panel.h"
#include "container.h"

static void container_behaviour_init   (AGlBehaviour*, AGlActor*);
static void container_behaviour_layout (AGlBehaviour*, AGlActor*);

static AGlBehaviourClass klass = {
	.new = container_behaviour,
	.init = container_behaviour_init,
	.layout = container_behaviour_layout
};


AGlBehaviourClass*
container_behaviour_get_class ()
{
	return &klass;
}


AGlBehaviour*
container_behaviour ()
{
	return AGL_NEW(AGlBehaviour,
		.klass = &klass,
	);
}


static void
container_behaviour_init (AGlBehaviour* behaviour, AGlActor* actor)
{
}


static void
container_behaviour_layout (AGlBehaviour* behaviour, AGlActor* actor)
{
	PF;

	typedef struct {
		AGlActor*       actor;
		PanelBehaviour* panel;
		float           width;
	} Item;

	float measure (int n, Item items[])
	{
		float a = 0;
		for(int i=0;i<n;i++){
			a += items[i].width;
		}
		return a;
	}

	float available_for_reduction (int n, Item items[])
	{
		float a = 0;
		for(int i=0;i<n;i++){
			if(items[i].panel->preferred > items[i].panel->min){
				a += items[i].panel->preferred - items[i].panel->min;
			}
		}
		return a;
	}

	float available_for_expansion (int n, Item items[])
	{
		float a = 0;
		for(int i=0;i<n;i++){
			a += MAX(0, items[i].panel->max - items[i].panel->preferred);
		}
		return a;
	}

	float space = agl_actor__width(actor);

	Item items[g_list_length(actor->children)];

	float x = 0.;
	int i = 0;
	int n_panels = 0;
	for(GList* l = actor->children; l; l = l->next){
		AGlActor* child = l->data;
		PanelBehaviour* panel = (PanelBehaviour*)agl_actor__find_behaviour(child, panel_get_class());
		if(panel){
			agl_actor__set_size(child);

			items[i] = (Item){
				.actor = child,
				.panel = panel,
				.width = panel->preferred
			};
			x += panel->preferred;
			i++;
			n_panels++;
		}
	}

	if(x > space){
		dbg(1, "content too big. need to reduce %.0f", (x - space));

		float _available_for_reduction = available_for_reduction(n_panels, items);
		if(_available_for_reduction > 0){
			float reduction_per_pixel = (x - space) / _available_for_reduction;
			g_return_if_fail(reduction_per_pixel < 1.);
			g_return_if_fail(reduction_per_pixel > 0.);
			float tot = 0;
			for(int i=0;i<n_panels;i++){
				if(items[i].panel->preferred > items[i].panel->min){
					float modifiable_amount = items[i].panel->preferred - items[i].panel->min;
					items[i].width -= modifiable_amount * reduction_per_pixel;
					tot += modifiable_amount * reduction_per_pixel;
				}
			}
			dbg(1, "total reduction: %.1f", tot);
		}

		x = measure(n_panels, items);
		if(x > space){
			dbg(0, "TODO need to reduce further, below min");
		}
	}

	if(x < space - 0.5){
		float amount_to_expand = space - x;
		dbg(1, "content small, does not fill space. short by %.0f", amount_to_expand);
		float _available_for_expansion = available_for_expansion(n_panels, items);
		if(_available_for_expansion > 0){
			float expansion_per_pixel = amount_to_expand / _available_for_expansion;

			for(int i=0;i<n_panels;i++){
				if(items[i].panel->preferred > items[i].panel->min){
					float modifiable_amount = items[i].panel->max - items[i].panel->preferred;
					items[i].width += modifiable_amount * expansion_per_pixel;
				}
			}
		}
	}

	x = 0.;
	for(int i=0;i<n_panels;i++){
		float w = items[i].width;
		items[i].actor->region = (AGlfRegion){x, 0, x + w, actor->region.y2};
		agl_actor__set_size(items[i].actor);
		x += w;
	}

	agl_actor__invalidate(actor);
}

