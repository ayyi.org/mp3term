/*
 +----------------------------------------------------------------------+
 | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
 | copyright (C) 2018-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include "debug/debug.h"
#include "behaviours/material.h"

static void material_init (AGlBehaviour*, AGlActor*);
static bool material_draw (AGlBehaviour*, AGlActor*, AGlActorPaint);

static AGlBehaviourClass klass = {
    .new = material,
	.init = material_init,
	.draw = material_draw,
};


AGlBehaviourClass*
material_get_class ()
{
	return &klass;
}


AGlBehaviour*
material ()
{
	MaterialBehaviour* a = AGL_NEW(MaterialBehaviour,
		.behaviour = {
			.klass = &klass,
		}
	);

	return (AGlBehaviour*)a;
}


static void
material_init (AGlBehaviour* behaviour, AGlActor* actor)
{
	MaterialBehaviour* material = (MaterialBehaviour*)behaviour;

	if(material->shader){
		agl_create_program(material->shader);
	}
}


static bool
material_draw (AGlBehaviour* behaviour, AGlActor* actor, AGlActorPaint wrapped)
{
	MaterialBehaviour* material = (MaterialBehaviour*)behaviour;

	if(material->shader){
		agl_use_program(material->shader);
	}
	bool good = wrapped (actor);

	return good;
}

