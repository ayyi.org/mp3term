/*
 +----------------------------------------------------------------------+
 | This file is part of Samplecat. https://ayyi.github.io/samplecat/    |
 | copyright (C) 2019-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  The panel behaviours allows nodes to be layed out in a container
 |  using the properties min, max, and preferred size. The node normally
 |  sets these properties in its layout handler.
 |
 */

#include "config.h"
#include "panel.h"

static void panel_behaviour_layout (AGlBehaviour*, AGlActor*);

static AGlBehaviourClass klass = {
	.new = panel_behaviour,
	.layout = panel_behaviour_layout
};


AGlBehaviourClass*
panel_get_class ()
{
	return &klass;
}


AGlBehaviour*
panel_behaviour ()
{
	return (AGlBehaviour*)AGL_NEW(PanelBehaviour,
		.behaviour = {
			.klass = &klass,
		},
	);
}


static void
panel_behaviour_layout (AGlBehaviour* behaviour, AGlActor* actor)
{
}

