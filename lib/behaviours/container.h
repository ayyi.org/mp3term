/**
* +----------------------------------------------------------------------+
* | This file is part of Samplecat. http://ayyi.github.io/samplecat/     |
* | copyright (C) 2016-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __container_behaviour_h__
#define __container_behaviour_h__

#include "glib.h"
#include "agl/actor.h"

AGlBehaviourClass* container_behaviour_get_class ();

AGlBehaviour*      container_behaviour ();

#endif
