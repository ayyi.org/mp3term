/**
* +----------------------------------------------------------------------+
* | This file is part of Mp3term. https://gitlab.com/ayyi.org/mp3term    |
* | copyright (C) 2018-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __behaviours_material_h__
#define __behaviours_material_h__

#include "glib.h"
#include "agl/actor.h"
#include "agl/behaviour.h"

typedef struct {
   AGlBehaviour behaviour;
   AGlShader*   shader;
} MaterialBehaviour;

AGlBehaviourClass* material_get_class ();

AGlBehaviour* material ();

#endif
