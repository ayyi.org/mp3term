/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2013-2023 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __ayyi_debug_h__
#define __ayyi_debug_h__

#include <stdio.h>
#include <inttypes.h>
#include <glib.h>

#ifdef DEBUG
// note that the level check is now outside the print fn in order to
// prevent functions that return print arguments from being run
#  define dbg(A, B, ...) do {if(A <= _debug_) debug_printf(__func__, A, B, ##__VA_ARGS__);} while(FALSE)

#  ifndef PF
#  define PF {if(_debug_) printf("%s()...\n", __func__);}
#  endif
#  define PF0 {printf("%s()...\n", __func__);}
#  define PF2 {if(_debug_ > 1) printf("%s...\n", __func__);}
#  define PF_DONE {if(_debug_) printf("%s(): done.\n", __func__);}
#else
#  define dbg(A, B, ...)
#  define PF
#  define PF0
#  define PF2
#  define PF_DONE
#endif
#define perr(A, ...) g_critical("%s(): "A, __func__, ##__VA_ARGS__)
#ifndef pwarn
#define pwarn(A, ...) g_warning("%s(): "A, __func__, ##__VA_ARGS__)
#endif

#ifdef DEBUG
void debug_printf     (const char* func, int level, const char* format, ...);
#endif
void warnprintf       (const char* format, ...);
void errprintf        (const char* fmt, ...);

#if 0
void set_log_handlers (char*[]);
#else
void set_log_handlers ();
#endif

#ifdef __debug_c__
char red[]    = "\x1b[1;31m";
char green[]  = "\x1b[1;32m";
char yellow[] = "\x1b[1;33m";
char bold[]   = "\x1b[1;39m";

#else
extern int _debug_;                  // debug level. 0=off.

extern char ayyi_white [];
extern char ayyi_grey  [16];
extern char ayyi_bold  [];
extern char ayyi_warn  [32];
extern char ayyi_err   [];
extern char green      [];
extern char red        [];
extern char yellow     [];
extern char bold       [];
#endif

#endif
