Mp3term is a terminal application that displays audio metadata for the current directory
such as cover artwork and waveforms.

It is intended for users who prefer the simplicity and flexibility of command line music players.

It works primarily on GNU/Linux systems.

![Screenshot](mp3term.gif "Screenshot")

To compile:
```
./autogen.sh
./configure
make
```

## Dependencies

Required dependencies are `gtk-2`, `vte`, `ffmpeg`, `libyaml`, `libgraphene`.

Optional dependencies

 - To use audio metering, please install `dbus-glib-1` and [Ayyi Alsa Meter](https://gitlab.com/ayyi.org/alsa-meter).
 - To show webp images, gdk-pixbuf-loader-webp is needed.
